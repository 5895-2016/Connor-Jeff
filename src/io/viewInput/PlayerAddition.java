/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package io.viewInput;

import java.io.Serializable;

import client.view.LobbyView;
import client.view.View;

public class PlayerAddition implements ViewInput, Serializable {
    
    private static final long serialVersionUID = 777L;

    private int visiblePlayers;
    
    /**
     * @param players the {@code PlayerModel} for each player sorted by ranking in the metagame.
     */
    public PlayerAddition(int visiblePlayers){
        this.visiblePlayers = visiblePlayers;
    }
    
    public int getVisiblePlayerCount(){ return visiblePlayers; }
    
    /**
     * {@inheritDoc}
     */
    public void applyInput(View v){
        if (!(v instanceof LobbyView)){
            System.out.println("VIEW RECEIVEING PLAYERADDITION IS NOT A LOBBYVIEW");
        }
        else{
            LobbyView lobbyView = (LobbyView) v;
            lobbyView.showPlayer(this);
        }
    }
    
}
