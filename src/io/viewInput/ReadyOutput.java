/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package io.viewInput;

import java.io.Serializable;

import client.view.LobbyView;
import client.view.ResultsView;
import client.view.View;
import client.view.preview.PreviewView;

public class ReadyOutput implements ViewInput, Serializable {
    
    private static final long serialVersionUID = 555L;
    
    private int playerNumber;
    
    /**
     * @param number The number representing the player who is ready.
     */
    public ReadyOutput(int number){
        playerNumber = number;
    }
    
    public int getPlayerNumber(){
        return playerNumber;
    }
    
    /**
     * {@inheritDoc}
     */
    public void applyInput(View v){
        if (!(v instanceof PreviewView) && !(v instanceof LobbyView) && !(v instanceof ResultsView)){
            System.out.println("VIEW RECEIVEING READYOUTPUT IS NOT A PREVIEWVIEW, RESULTSVIEW, OR LOBBYVIEW");
        } else if (v instanceof LobbyView){
        	LobbyView lobbyView = (LobbyView) v;
        	lobbyView.updateReadyCheck(this);
        } else if(v instanceof PreviewView) {
            PreviewView previewView = (PreviewView) v;
            previewView.previewUpdate(this);
        } else if(v instanceof ResultsView) {
        	ResultsView resultsView = (ResultsView) v;
        	resultsView.updateReadyCheck(this);
        }
    }
}
