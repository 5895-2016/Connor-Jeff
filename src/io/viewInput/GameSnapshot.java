/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package io.viewInput;

import java.io.Serializable;

import client.view.View;
import client.view.game.GameView;
import server.model.entity.Entity;

public class GameSnapshot implements ViewInput, Serializable {
    
    private static final long serialVersionUID = 222L;

    private Entity[] players;
    private Entity[] inanimates;
    private Entity[] staticObjects;
    
    /**
     * @param players Entities representing the players.
     * @param inanimates Entities representing things that are not players.
     */
    public GameSnapshot(Entity[] players, Entity[] inanimates, Entity[] staticObjects){
        this.players = players;
        this.inanimates = inanimates;
        this.staticObjects = staticObjects;
    }
    
    public Entity[] getPlayerEntities(){
        return players;
    }
    
    public Entity[] getInanimateEntities(){
        return inanimates;
    }
    
    public Entity[] getStaticEntities(){
        return staticObjects;
    }
    
    /**
     * {@inheritDoc}
     */
    public void applyInput(View v){
        if (!(v instanceof GameView)){
            System.out.println("VIEW RECEIVEING GAMESNAPSHOT IS NOT A GAMEVIEW");
        }
        else{
            GameView gameView = (GameView) v;
            gameView.gameUpdate(this);
        }
    }

}
