/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package io.viewInput;

import client.view.View;

public interface ViewInput {
    
    /**
     * Modifies the view using the input data.
     * Usually calls a view method passing {@code this} as an argument.
     * @param v the view on which the input is applied.
     */
    public void applyInput(View v);

}
