/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package io.viewInput;

import java.io.Serializable;

import client.view.View;
import client.view.preview.PreviewView;
import server.model.entity.Entity;

public class PreviewSnapshot implements ViewInput, Serializable {
    
    private static final long serialVersionUID = 444L;

    private Entity[] players;
    private Entity[] inanimates;
    
    /**
     * @param players Entities representing the players.
     * @param inanimates Entities representing things that are not players.
     */
    public PreviewSnapshot(Entity[] players, Entity[] inanimates){
        this.players = players;
        this.inanimates = inanimates;
    }
    
    public Entity[] getPlayerEntities(){
        return players;
    }
    
    public Entity[] getInanimateEntities(){
        return inanimates;
    }
    
    /**
     * {@inheritDoc}
     */
    public void applyInput(View v){
        if (!(v instanceof PreviewView)){
            System.out.println("VIEW RECEIVEING PREVIEWSNAPSHOT IS NOT A PREVIEWVIEW");
        }
        else{
            PreviewView previewView = (PreviewView) v;
            previewView.animationUpdate(this);
        }
    }

}
