/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package io.viewInput;

import client.view.View;
import client.view.WinscreenView;
import server.model.PlayerModel;

public class WinscreenOutput implements ViewInput {
    
    private PlayerModel[] players;
    
    public WinscreenOutput(PlayerModel[] players){
        this.players = players;
    }
    
    public PlayerModel[] getPlayers(){
        return this.players;
    }

    @Override
    public void applyInput(View v) {
        if (!(v instanceof WinscreenView)){
            System.out.println("VIEW RECEIVEING LOBBYOUTPUT IS NOT A WINSCREENVIEW");
        }
        else{
            WinscreenView view = (WinscreenView) v;
            view.winscreenUpdate(this);
        }
    }

}
