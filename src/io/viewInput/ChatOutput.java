/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package io.viewInput;

import java.io.Serializable;

import client.view.LobbyView;
import client.view.View;

public class ChatOutput implements ViewInput, Serializable {
    
    private static final long serialVersionUID = 111L;

    private String chatMessage;
    
    /**
     * @param message The chat message being sent.
     */
    public ChatOutput(String message){
        chatMessage = message;
    }

    public String getChatMessage(){ return this.chatMessage; }
    
    /**
     * {@inheritDoc}
     */
    public void applyInput(View v){
        if (!(v instanceof LobbyView)){
            System.out.println("VIEW RECEIVEING CHATOUTPUT IS NOT A LOBBYVIEW");
        }
        else{
            LobbyView previewView = (LobbyView) v;
            previewView.chatUpdate(this);
        }
    }
    
}
