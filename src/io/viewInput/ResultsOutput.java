/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package io.viewInput;

import server.model.PlayerModel;

import java.io.Serializable;

import client.view.ResultsView;
import client.view.View;

public class ResultsOutput implements ViewInput, Serializable {
    
    private static final long serialVersionUID = 666L;
    
    private PlayerModel players[];
    
    /**
     * @param players array of each player's {@code PlayerModel} sorted by score in the previous minigame.
     */
    public ResultsOutput(PlayerModel players[]){
        this.players = players;
    }
    
    public PlayerModel[] getPlayers(){ return players; }
    
    /**
     * {@inheritDoc}
     */
    public void applyInput(View v){
        if (!(v instanceof ResultsView)){
            System.out.println("VIEW RECEIVEING RESULTSOUTPUT IS NOT A RESULTSVIEW");
        }
        else{
            ResultsView resultsView = (ResultsView) v;
            resultsView.resultsUpdate(this);
        }
    }
    
}
