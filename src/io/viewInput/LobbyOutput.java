/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package io.viewInput;

import server.model.PlayerModel;

import java.io.Serializable;

import client.view.LobbyView;
import client.view.View;

public class LobbyOutput implements ViewInput, Serializable {
    
    private static final long serialVersionUID = 333L;

	private PlayerModel players[];
	
	/**
	 * @param players the {@code PlayerModel} for each player sorted by ranking in the metagame.
	 */
	public LobbyOutput(PlayerModel players[]){
	    this.players = players;
	}
	
	public PlayerModel[] getPlayers(){ return players; }
	
	/**
     * {@inheritDoc}
     */
    public void applyInput(View v){
        if (!(v instanceof LobbyView)){
            System.out.println("VIEW RECEIVEING LOBBYOUTPUT IS NOT A LOBBYVIEW");
        }
        else{
            LobbyView lobbyView = (LobbyView) v;
            lobbyView.lobbyUpdate(this);
        }
    }
	
}
