/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package io.modelInput;

import java.io.Serializable;

import server.model.Model;
import server.model.game.GameModel;
import server.model.preview.PreviewModel;

public class ModelTimerInput implements ModelInput, Serializable {
    
    private static final long serialVersionUID = 4444L;
    
    private long millis;
    
    /**
     * @param millis The elapsed time in milliseconds.
     */
    public ModelTimerInput(long millis){
        this.millis = millis;
    }
    
    public long getMillis(){
        return millis;
    }
    
    /**
     * {@inheritDoc}
     */
    public void applyInput(Model m){
        if (!(m instanceof GameModel || m instanceof PreviewModel)){
            System.out.println("MODEL RECEIVEING MODELTIMERINPUT IS NOT A GAMEMODEL OR PREVIEWMODEL");
        }
        else if (m instanceof GameModel){
            GameModel model = (GameModel) m;
            model.modelTimerUpdate(this);
        }
        else{
            PreviewModel model = (PreviewModel) m;
            model.modelTimerUpdate(this);
        }
    }
}
