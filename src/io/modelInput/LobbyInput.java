/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package io.modelInput;

import java.io.Serializable;

import server.model.LobbyModel;
import server.model.Model;

public class LobbyInput implements ModelInput, Serializable {
    
    private static final long serialVersionUID = 3333L;

	public int playerScore;
	public int playerNumber;
     
	/**
	 * @param score The score to add to this player's total score.
	 * @param playerNumber The number representing the player.
	 */
    public LobbyInput(int score, int playerNumber){
    	this.playerNumber = playerNumber;
    	this.playerScore = score;
    }
    
    /**
     * {@inheritDoc}
     */
    public void applyInput(Model m){
        if (!(m instanceof LobbyModel)){
            System.out.println("MODEL RECEIVEING LOBBYINPUT IS NOT A LOBBYMODEL");
        }
        else{
            LobbyModel model = (LobbyModel) m;
            model.lobbyUpdate(this);
        }
    }

}
