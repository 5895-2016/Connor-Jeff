/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package io.modelInput;

import java.io.Serializable;

import server.model.LobbyModel;
import server.model.Model;

public class ChatInput implements ModelInput, Serializable {
    
    private static final long serialVersionUID = 1111L;

    private String chatMessage;
    
    /**
     * @param message The chat message being sent.
     */
    public ChatInput(String message){
        chatMessage = message;
    }
    
    public String getChatMessage(){
        return this.chatMessage;
    }
    
    /**
     * {@inheritDoc}
     */
    public void applyInput(Model m){
        if (!(m instanceof LobbyModel)){
            System.out.println("MODEL RECEIVEING LOBBYINPUT IS NOT A LOBBYMODEL");
        }
        else{
            LobbyModel model = (LobbyModel) m;
            model.chatUpdate(this);
        }
    }
}
