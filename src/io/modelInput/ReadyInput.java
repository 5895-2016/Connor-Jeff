/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package io.modelInput;

import java.io.Serializable;

import server.model.LobbyModel;
import server.model.Model;
import server.model.ResultsModel;
import server.model.preview.PreviewModel;

public class ReadyInput implements ModelInput, Serializable {
    
    private static final long serialVersionUID = 5555L;
    
    private int playerNumber;
    
    /**
     * @param number The number representing the player sending the input.
     */
    public ReadyInput(int number){
        playerNumber = number;
    }
    
    public int getPlayerNumber(){
        return playerNumber;
    }
    
    /**
     * {@inheritDoc}
     */
    public void applyInput(Model m){
        if (!(m instanceof PreviewModel) && !(m instanceof LobbyModel) && !(m instanceof ResultsModel)){
            System.out.println("MODEL RECEIVEING READYINPUT IS NOT A LOBBYMODEL, RESULTSMODEL, OR PREVIEWMODEL");
        } else if (m instanceof LobbyModel){
        	LobbyModel model = (LobbyModel) m;
        	model.updateReadyCheck(this);
        } else if (m instanceof PreviewModel){
            PreviewModel model = (PreviewModel) m;
            model.previewUpdate(this);
        } else{
            ResultsModel model = (ResultsModel) m;
            model.resultsUpdate(this);
        }
    }
    
}
