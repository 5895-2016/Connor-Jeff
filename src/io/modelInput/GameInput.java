/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package io.modelInput;

import java.awt.Point;
import java.io.Serializable;

import server.model.Model;
import server.model.game.GameModel;

public class GameInput implements ModelInput, Serializable {
    
    private static final long serialVersionUID = 2222L;

    private boolean up;
    private boolean down;
    private boolean left;
    private boolean right;
    private boolean space;
    private int playerNumber;
    private Point mouseMovePosition;
    private Point mouseClickPosition;
    private boolean clicked;
    
    /**
     * @param up true if the player is holding up.
     * @param down true if the player is holding down.
     * @param left true if the player is holding left.
     * @param right true if the player is holding right.
     * @param playerNumber The number representing the player.
     */
    public GameInput(boolean up, boolean down, boolean left, boolean right, boolean space, Point mousePosition, int playerNumber){

        this.up = up;
        this.down = down;
        this.left = left;
        this.right = right;
        this.mouseMovePosition = mousePosition;
        this.space = space;
        this.playerNumber = playerNumber;
        this.mouseClickPosition = new Point(101,101);
        this.clicked = false;
    }
    
    public GameInput(boolean up, boolean down, boolean left, boolean right, boolean space, Point mousePosition, Point mouseClickPosition, int playerNumber, boolean clicked){
        this.up = up;
        this.down = down;
        this.left = left;
        this.right = right;
        this.mouseMovePosition = mousePosition;
        this.space = space;
        this.playerNumber = playerNumber;
        this.mouseClickPosition = mouseClickPosition;
        this.clicked = clicked;
    }
    
    public boolean up(){
        return up;
    }
    
    public boolean down(){
        return down;
    }
    
    public boolean left(){
        return left;
    }
    
    public boolean right(){
        return right;
    }
    
    public boolean space(){
        return space;
    }
    
    public int getPlayerNumber(){
        return playerNumber;
    }
    
    public Point getMousePos(){
    	return mouseMovePosition;
    }
    
    public Point getMouseClickPos(){
    	return mouseClickPosition;
    }
    
    public boolean getClicked(){
    	return clicked;
    }
    
    /**
     * {@inheritDoc}
     */
    public void applyInput(Model m){
        if (!(m instanceof GameModel)){
            System.out.println("MODEL RECEIVEING GAMEINPUT IS NOT A GAMEMODEL");
        }
        else{
            GameModel model = (GameModel) m;
            model.gameUpdate(this);
        }
    }

}
