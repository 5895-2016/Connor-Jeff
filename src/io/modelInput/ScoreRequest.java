/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package io.modelInput;

import java.io.Serializable;

import server.model.LobbyModel;
import server.model.Model;
import server.model.ResultsModel;
import server.model.WinscreenModel;

public class ScoreRequest implements ModelInput, Serializable {
    
    private static final long serialVersionUID = 6666L;
    
    public ScoreRequest(){}
    
    /**
     * {@inheritDoc}
     */
    public void applyInput(Model m){
        if (!(m instanceof LobbyModel || m instanceof ResultsModel || m instanceof WinscreenModel)){
        System.out.println("MODEL RECEIVEING SCOREREQUEST IS NOT A LOBBYMODEL, RESULTSMODEL, OR WINSCREENMODEL");
        }
        else if (m instanceof LobbyModel){
            LobbyModel model = (LobbyModel) m;
            model.scoreUpdate(this);
        }
        else if (m instanceof ResultsModel){
            ResultsModel model = (ResultsModel) m;
            model.scoreUpdate(this);
        }
        else{
            WinscreenModel model = (WinscreenModel) m;
            model.scoreUpdate(this);
        }
    }
}
