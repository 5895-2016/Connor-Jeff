/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package io.modelInput;

import server.model.Model;

public interface ModelInput {
    
    /**
     * Modifies the model using the input data.
     * Usually calls a model method passing {@code this} as an argument.
     * @param m the model on which the input is applied.
     */
    public void applyInput(Model m);
    
}
