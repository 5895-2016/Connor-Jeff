/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

import client.Client;
import client.ClientInterface;
import game.GameState;
import game.MiniGame;
import io.modelInput.ModelInput;
import io.viewInput.ViewInput;
import server.model.LobbyModel;
import server.model.Model;
import server.model.ModelFactory;
import server.model.PlayerModel;

public class Server extends UnicastRemoteObject implements ServerInterface {
	
    private static final long serialVersionUID = 1000L;
    
    public static int port = 8765;
    public static String networkName = "server";
    
	private GameState currentState;
    private Model currentModel;
    private LobbyModel lobbyModel;
	private ModelFactory modelFactory;
	private Vector <ClientInterface> clients;
	private boolean[] clientReady;
	private String[] playerNames = {"player1","player2","player3","player4"};
	private PlayerModel[] currentGameScore;
	
	/**
	 * Constructor for Server
	 * @param client - The host client that created the server.
	 */
	public Server(Client client, String name) throws RemoteException{
		clients = new Vector<ClientInterface>();
	    modelFactory = new ModelFactory(this);
		clientReady = new boolean[] {false, true, true, true};
		clients.add(client);
		client.setPlayerNumber(1);
		playerNames[0] = name;
		
	    Registry registry = LocateRegistry.createRegistry(port);
	    registry.rebind(networkName, this);
		
		this.setState(GameState.GAMELOBBY);
		lobbyModel = (LobbyModel) currentModel;

	}
	
	// Old, non-remote way of creating server for testing purposes
	public Server(Client client, int num) throws RemoteException{
        clients = new Vector<ClientInterface>();
        modelFactory = new ModelFactory(this);
        clientReady = new boolean[] {false, true, true, true};
        clients.add(client);
        client.setPlayerNumber(1);
        
        this.setState(GameState.GAMELOBBY);
        lobbyModel = (LobbyModel) currentModel;
	}
	
	public void addClient(ClientInterface c, String name){
	    if (clients.size() < 4){
	        try {
                c.setPlayerNumber(clients.size()+1);
                clients.add(c);
                clientReady[clients.size()-1] = false;
                c.changeState(currentState);
                this.playerNames[clients.size()-1] = name;
                lobbyModel.addPlayer();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
	    } else{
	        throw new ArrayIndexOutOfBoundsException();
	    }
	}
	
    /**
     * Change the current state the new state.
     * @param newState
     */
	private void setState(GameState newState){
	    if (currentState == null || newState != currentState){
	        stopSending();
	        
	        currentState = newState;
	        if (newState == GameState.GAMELOBBY && lobbyModel != null){
	            currentModel = lobbyModel;
	        }
	        else{
	            currentModel = modelFactory.create(currentState);
	        }
	        
    	    for (ClientInterface c : clients){
    	        Thread clientStateThread = new Thread(new Runnable(){
    	           public void run(){
    	               try {
                        c.changeState(newState);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
    	           }
    	        });
    	        clientStateThread.start();
    	    }
	    }
	}
	
	/**
	 * Sets all clients to not ready for update.
	 */
	synchronized private void stopSending(){
	    for (int i = 0; i < clients.size(); i++){
	        clientReady[i] = false;
	    }
	}
	
	/**
	 * Sets all clients to ready for update.
	 */
	synchronized public void clientReady(int playerNumber){
	    clientReady[playerNumber - 1] = true;
	    notifyAll();
	}
	
	/**
	 * Updates the current model with input from the client.
	 * @param modelInput
	 */
	public void updateModel(ModelInput modelInput){
		currentModel.updateModel(modelInput);
	}
	
	/**
	 * Updates each client with output from the current model.
	 * @param viewInput
	 */
	synchronized public void updateClients(ViewInput viewInput){
	    while (!Arrays.equals(clientReady, new boolean[]{true, true, true, true})){
	        try{
	            wait();
	        } catch (InterruptedException e){
	            e.printStackTrace();
	        }
	    }
		for (ClientInterface c : clients){
		    Thread clientUpdateThread = new Thread(new Runnable(){
		        public void run(){
		            try {
                        c.updateView(viewInput);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
		        }
		    });
		    clientUpdateThread.start();
		}
	}
	
	synchronized public void setCurrentMinigame(MiniGame game){
        for (ClientInterface c : clients){
            Thread clientUpdateThread = new Thread(new Runnable(){
                public void run(){
                    try {
                        c.setCurrentMinigame(game);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            });
            clientUpdateThread.start();
        }
	}
	
	/**
	 * Updates the lobby model.
	 * @param input
	 */
	public void updateLobby(ModelInput input){
	    lobbyModel.updateModel(input);
	}
    
    public void startLobby(){
        setState(GameState.GAMELOBBY);
    }
    
    public void startPreview(){
        modelFactory.chooseNewMinigame();
        setState(GameState.PREVIEW);
    }
    
    public void startGame(){
        setState(GameState.MINIGAME);
    }
    
    public void startResults(PlayerModel[] results){
        setCurrentGameScore(results);
        setState(GameState.RESULTS);
    }
    
    public void endResults(){
        if (modelFactory.finished()){
            setState(GameState.WINSCREEN);
        } else{
            setState(GameState.GAMELOBBY);
        }
    }
    
    public void setCurrentGameScore(PlayerModel[] results){ this.currentGameScore = results; }
    
    public PlayerModel[] getCurrentGameScore(){ return this.currentGameScore; }
    
    public PlayerModel[] getMetagameResults(){ return lobbyModel.getSortedPlayers(); }
    
    public String getPlayerName(int playerNumber){
    	return playerNames[playerNumber - 1];
    }
}
