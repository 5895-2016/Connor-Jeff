/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server;

import java.rmi.Remote;
import java.rmi.RemoteException;

import client.ClientInterface;
import io.modelInput.ModelInput;

public interface ServerInterface extends Remote {
    
    public void addClient(ClientInterface c, String name) throws RemoteException;
    
    public void clientReady(int playerNumber) throws RemoteException;
    
    public void updateModel(ModelInput modelInput) throws RemoteException;

}
