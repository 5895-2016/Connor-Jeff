/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model;

public class ExplosionCounter {

	private int count;
	
	public ExplosionCounter(){
		count = 0;
	}
	
	public int getCount(){ return count; }
	public void increment(){ count++; }
	
}
