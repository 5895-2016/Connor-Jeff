/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model;

import io.modelInput.ScoreRequest;
import io.viewInput.WinscreenOutput;
import server.Server;

public class WinscreenModel extends Model {
    
    private PlayerModel[] sortedPlayers;
    
    public WinscreenModel(Server server){
        super(server);
        this.server = server;
        sortedPlayers = server.getMetagameResults();
    }
    
    /**
     * Sends the current score to the clients.
     * @param s Input to the clients with updated scores.
     */
    public void scoreUpdate(ScoreRequest s){
        WinscreenOutput output = new WinscreenOutput(sortedPlayers);
        updateClients(output);
    }

}
