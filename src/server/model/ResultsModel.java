/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model;

import java.util.Arrays;

import io.modelInput.ReadyInput;
import io.modelInput.ScoreRequest;
import io.viewInput.ReadyOutput;
import io.viewInput.ResultsOutput;
import io.viewInput.ViewInput;
import server.Server;

public class ResultsModel extends Model{

    private Server server;
    
	PlayerModel[] players;
    private boolean[] playerReady;
    
    
	/**
	 * Constructor for results
	 * @param server - The server that creates this model
	 */
	public ResultsModel(Server server){
		super(server);
		this.server = server;
        playerReady = new boolean[] {false, false, false, false};
		players = server.getCurrentGameScore();
		
	}
    
	/**
	 * Sends score update to clients
	 * @param s
	 */
    public void scoreUpdate(ScoreRequest s){
        ResultsOutput resultsOutput = new ResultsOutput(players);
        updateClients(resultsOutput);
    }
    
    /**
     * Updates model with player that is ready
     * @param readyInput
     */
    public void resultsUpdate(ReadyInput readyInput){
        int playerNumber = readyInput.getPlayerNumber();
        playerReady[playerNumber-1] = true;
        ViewInput output = new ReadyOutput(playerNumber);
        updateClients(output);
        if (Arrays.equals(playerReady, new boolean[]{true, true, true, true})){
            endResults();
        }
    }
    
    /**
     * Changes state back to lobby
     */
    private void endResults(){
        server.endResults();
    }
}
