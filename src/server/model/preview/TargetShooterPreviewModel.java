/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.preview;

import java.util.Random;
import java.util.Vector;

import io.modelInput.ModelTimerInput;
import server.Server;
import server.model.ExplosionCounter;
import server.model.entity.Entity;
import server.model.entity.EntityFactory;
import server.model.entity.ImmobileTarget;
import server.model.entity.MobileCrosshair;
import server.model.entity.ModelExplosion;
import server.model.entity.TargetShooterFactory;

public class TargetShooterPreviewModel extends PreviewModel {
	
	private TargetShooterFactory entityFactory;
	private MobileCrosshair[] crosshairs;
	private Vector <ImmobileTarget> targets;
	private Vector <ModelExplosion> explosions;
	private Vector <ExplosionCounter> explosionCounters;
	
	private int crosshairMoveTick;
	private int explosionTime;
	private int randomDir;
	private int counter;
	private int limit;
	private double nextX[];
	private double nextY[];
	private int[] xMove = {1,-1,1,1};
	private int[] yMove = {-1,1,-1,1};
	
	public TargetShooterPreviewModel(Server server) {
		super(server);
		entityFactory = new TargetShooterFactory();
		targets = new Vector<ImmobileTarget>();
		explosions = new Vector <ModelExplosion>();
		explosionCounters = new Vector <ExplosionCounter>();
		crosshairs = entityFactory.createPlayers();
		players = crosshairs;
		explosionTime = randomNum(100,600);
		randomDir = randomNum(50,150);
		counter = 0;
		crosshairMoveTick = 0;
		limit = 10;
		clock.start();
		nextX = new double[4];
		nextY = new double[4];
		for (int i = 0; i < 3; i++){
			int x = randomNum(5, 45);
			int y = randomNum(5, 45);
			crosshairs[i].setPosition(x, y);
			nextX[i] = x;
			nextY[i] = y;
		}
		
		crosshairs[3].setPosition(-5, -5);
		
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void modelTimerUpdate(ModelTimerInput tick) {
		crosshairMoveTick++;

		if((crosshairMoveTick % 15) == 0){
			for (int i = 0; i < 4; i++){
				if (nextX[i] < 0){
					xMove[i] = -xMove[i];
					crosshairs[i].setPosition(1, nextY[i]);
					nextX[i] = 1;
				} else  if (nextX[i] > EntityFactory.WorldWidth){
					xMove[i] = -xMove[i];
					crosshairs[i].setPosition(99, nextY[i]);
					nextX[i] = 99;
				} else  if (nextY[i] < 0){
					yMove[i] = -(yMove[i]);
					crosshairs[i].setPosition(nextX[i], 1);
					nextY[i] = 1;
				} else if (nextY[i] > EntityFactory.WorldHeight){
					yMove[i] = -(yMove[i]);
					crosshairs[i].setPosition(nextX[i], 99);
					nextY[i] = 99;
				} else {
					crosshairs[i].setPosition(nextX[i], nextY[i]);
				}
				nextX[i] = nextX[i] + xMove[i];
				nextY[i] = nextY[i] + yMove[i];
			}
			
		}
		
		if(crosshairMoveTick == randomDir){
			int num = randomNum(0,5);
			if (randomDir > 100){
				xMove[num] = -xMove[num];
			} else {
				yMove[num] = -yMove[num];
			}
		}
		
		if(crosshairMoveTick == explosionTime){
			int num = randomNum(0,5);
			explosions.add(new ModelExplosion((int)nextX[num],(int)nextY[num]));
			explosionCounters.add(new ExplosionCounter());
			explosionTime = randomNum(100,600);
		}
		
		if (crosshairMoveTick % 400 == 0){
			if (counter < limit){
				targets.addElement(entityFactory.addTarget(randomNum(10,90), randomNum(10,90), 10, 10));
				counter++;
			}
		}
		
		if (crosshairMoveTick == 600){
			crosshairMoveTick = 0;
		}
		
		if (explosionCounters.size() != 0){
			for (ExplosionCounter c : explosionCounters){
				c.increment();
				if (c.getCount() == 20){
					explosions.remove(0);
				}
			}
		}

	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public void packageInanimateEntities() {
		inanimates = new Entity[targets.size() + explosions.size()];
		for (int i = 0; i < targets.size(); i++){
			inanimates[i] = targets.elementAt(i);
		}
		for (int i = 0; i < explosions.size(); i++){
			inanimates[i+targets.size()] = explosions.elementAt(i);
		}
	}
	
	/**
	 * Returns a random integer within a given range.
	 * @param min Minimum value of range
	 * @param max Maximum value of range
	 * @return a random integer within a given range
	 */
	private int randomNum(int min, int max){
		Random rand = new Random();
		return rand.nextInt((max-min)-1) + min;
	}

	/**
	 * Adds a target to the preview at a random location.
	 */
	public void addTarget(){
		targets.addElement(entityFactory.addTarget(randomNum(10,90), randomNum(10,90), 10, 10));
	}
	
}
