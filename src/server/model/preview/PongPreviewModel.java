/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.preview;

import java.util.Iterator;
import java.util.Vector;

import io.modelInput.ModelTimerInput;
import server.Server;
import server.model.entity.Entity;
import server.model.entity.EntityFactory;
import server.model.entity.ImmobileRectangle;
import server.model.entity.MobileCircle;
import server.model.entity.MobileRectangle;
import server.model.entity.PongFactory;

public class PongPreviewModel extends PreviewModel {
    
    static public double PADDLESPEED = 0.02;

    private MobileRectangle[] paddles;
    private ImmobileRectangle[] corners;
    private Vector<MobileCircle> balls;
    private PongFactory entityFactory;
    
    public PongPreviewModel(Server server){
        super(server);

        entityFactory = new PongFactory();
        paddles = entityFactory.createPlayers();
        corners = entityFactory.createCorners();
        balls = new Vector<MobileCircle>();
        for (int i = 0; i < 4; i++){
            entityFactory.addBall(balls);
        }
        
        players = paddles;
        inanimates = new Entity[]{};

        paddles[0].setYSpeed(PADDLESPEED);
        paddles[1].setXSpeed(PADDLESPEED);
        paddles[2].setYSpeed(-PADDLESPEED);
        paddles[3].setXSpeed(-PADDLESPEED);

        clock.start();
    }
    
    /**
     * {@inheritDoc}
     */
    public void modelTimerUpdate(ModelTimerInput tick){
        for (MobileRectangle r : paddles){
            double newX = r.getX()+(tick.getMillis() * r.getXSpeed());
            double newY = r.getY()+(tick.getMillis() * r.getYSpeed());
            r.setPosition(newX, newY);
            for (ImmobileRectangle corner : corners){
                if (corner.collide(r)){
                    corner.moveOut(r);
                    r.setXSpeed(-r.getXSpeed());
                    r.setYSpeed(-r.getYSpeed());
                }
            }
        }
        Iterator<MobileCircle> ballIterator = balls.iterator();
        int newBalls = 0;
        while (ballIterator.hasNext()){
            MobileCircle b = ballIterator.next();
            double newX = b.getX()+(tick.getMillis() * b.getXSpeed());
            double newY = b.getY()+(tick.getMillis() * b.getYSpeed());
            b.setPosition(newX, newY);
            for (ImmobileRectangle corner : corners){
                if (corner.collide(b)){
                    corner.deflect(b);
                }
            }
            for (MobileRectangle paddle : paddles){
                if (paddle.collide(b)){
                    paddle.deflect(b);
                }
            }
            if (b.getX() > EntityFactory.WorldWidth || b.getX()+b.getWidth() < 0 ||
                    b.getY() > EntityFactory.WorldHeight || b.getY()+b.getHeight() < 0){
                newBalls += 1;
                ballIterator.remove();
            }
        }
        for (int i = 0; i < newBalls; i++){
            entityFactory.addBall(balls);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    public void packageInanimateEntities(){
        inanimates = new Entity[corners.length + balls.size()];
        for (int i = 0; i < corners.length; i++){
            inanimates[i] = corners[i];
        }
        for (int i = corners.length; i < corners.length+balls.size(); i++){
            inanimates[i] = balls.elementAt(i-corners.length);
        }
    }

}
