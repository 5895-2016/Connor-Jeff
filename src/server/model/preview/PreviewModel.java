/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.preview;

import java.util.Arrays;

import io.modelInput.ModelTimerInput;
import io.modelInput.ReadyInput;
import io.modelInput.ViewTimerInput;
import io.viewInput.PreviewSnapshot;
import io.viewInput.ReadyOutput;
import io.viewInput.ViewInput;
import server.Server;
import server.model.Model;
import server.model.ModelClock;
import server.model.entity.Entity;


public abstract class PreviewModel extends Model {
    
    private Server server;
    
    private boolean[] playerReady;
    protected ModelClock clock;
    
    protected Entity[] players;
    protected Entity[] inanimates;
    
    /**
     * Constructor for PreviewModel
     * @param server - The Server that creates this model.
     */
    public PreviewModel(Server server){
        super(server);
        this.server = server;
        
        playerReady = new boolean[] {false, false, false, false};
        
        clock = new ModelClock(this);
    }
    
    /**
     * Updates the model with who is ready and notifies each client.
     * @param previewInput
     */
    public void previewUpdate(ReadyInput previewInput){
        int playerNumber = previewInput.getPlayerNumber();
        playerReady[playerNumber-1] = true;
        ViewInput output = new ReadyOutput(playerNumber);
        updateClients(output);
        if (Arrays.equals(playerReady, new boolean[]{true, true, true, true})){
            endPreview();
        }
    }
    
    /**
     * Updates the positions of each entity using its speed and elapsed time.
     * @param tick - Model input that contains the elapsed time.
     */
    public abstract void modelTimerUpdate(ModelTimerInput tick);
    
    /**
     * Updates the clients' views with the new entity locations.
     * @param tick - View input that contains elapsed time.
     */
    public void viewTimerUpdate(ViewTimerInput tick){
        packageInanimateEntities();
        PreviewSnapshot snapshot = new PreviewSnapshot(players, inanimates);
        updateClients(snapshot);
    }
    
    /**
     * packages the {@code inanimates} array with the inanimate entities to be drawn on the screen.
     */
    public abstract void packageInanimateEntities();
    
    /**
     * Disposes of this preview model and changes state to game
     */
    synchronized private void endPreview(){
        clock.dispose();
        server.startGame();
    }

}
