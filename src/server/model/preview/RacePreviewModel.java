/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.preview;

import io.modelInput.ModelTimerInput;
import server.Server;
import server.model.entity.EntityFactory;
import server.model.entity.ImmobileRectangle;
import server.model.entity.MobileRectangle;
import server.model.entity.RaceFactory;

public class RacePreviewModel extends PreviewModel {
    
    private RaceFactory entityFactory;
    private MobileRectangle[] rectangles;
    private ImmobileRectangle[] walls;
    
    public RacePreviewModel(Server server){
        super(server);
        
        entityFactory = new RaceFactory();
        
        rectangles = entityFactory.createPlayers();
        rectangles[0].setYSpeed(0.04);
        rectangles[1].setYSpeed(-0.04);
        rectangles[2].setYSpeed(0.04);
        rectangles[3].setYSpeed(-0.04);
        players = rectangles;
        
        walls = entityFactory.createWalls();
        
        clock.start();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void modelTimerUpdate(ModelTimerInput tick) {
        for (MobileRectangle r : rectangles){
            double newX = r.getX()+(tick.getMillis() * r.getXSpeed());
            double newY = r.getY()+(tick.getMillis() * r.getYSpeed());
            if (newY > EntityFactory.WorldHeight-r.getHeight()){
                newY = EntityFactory.WorldHeight-r.getHeight();
                r.setYSpeed(-r.getYSpeed());
            } else if (newY < 0){
                newY = 0;
                r.setYSpeed(-r.getYSpeed());
            }
            if (newX > RaceFactory.EndDistance){
                newX = 0;
            }
            r.setPosition(newX, newY);
            for (ImmobileRectangle wall : walls){
                if (wall.collide(r)){
                    wall.moveOut(r);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void packageInanimateEntities() {
        inanimates = walls;
    }

}