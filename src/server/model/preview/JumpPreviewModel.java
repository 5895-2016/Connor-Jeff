/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.preview;

import java.util.Timer;
import java.util.TimerTask;

import io.modelInput.ModelTimerInput;
import server.Server;
import server.model.entity.Entity;
import server.model.entity.EntityFactory;
import server.model.entity.ImmobileRectangle;
import server.model.entity.JumpFactory;
import server.model.entity.MobileRectangle;

public class JumpPreviewModel extends PreviewModel {
    
    public static double FallSpeed = 0.08;

    private JumpFactory entityFactory;
    private MobileRectangle[] jumpers;
    private ImmobileRectangle[] immobilePlatforms;
    private MobileRectangle[] mobilePlatforms;
    private ImmobileRectangle endToken;
    private Timer[] jumpTimers;
    private JumperState[] jumperStates;
    private int jumpCounter;
    
    public JumpPreviewModel(Server server){
        super(server);
        
        entityFactory = new JumpFactory();
        
        jumpers = entityFactory.createPlayers();
        players = jumpers;

        jumpers[0].setXSpeed(0.04);
        jumpers[1].setXSpeed(-0.04);
        jumpers[2].setXSpeed(0.04);
        jumpers[3].setXSpeed(-0.04);
        
        immobilePlatforms = entityFactory.createImmobilePlatforms();
        
        mobilePlatforms = entityFactory.createMobilePlatforms();
        
        endToken = entityFactory.createEndToken();
        
        jumperStates = new JumperState[]{JumperState.SIT, JumperState.SIT, JumperState.SIT, JumperState.SIT};
        jumpTimers = new Timer[4];
        
        jumpCounter = 0;
        
        clock.start();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void modelTimerUpdate(ModelTimerInput tick) {
        jumpCounter += tick.getMillis();
        if (jumpCounter >= 900){
            for (int i = 0; i < 4; i++){
                if (jumperStates[i] == JumperState.SIT){
                    startJump(i);
                }
            }
        }
        for (int i = 0; i < mobilePlatforms.length; i++){
            double newX = mobilePlatforms[i].getX()+(tick.getMillis() * mobilePlatforms[i].getXSpeed());
            double newY = mobilePlatforms[i].getY()+(tick.getMillis() * mobilePlatforms[i].getYSpeed());
            if (newX > EntityFactory.WorldWidth-mobilePlatforms[i].getWidth() || newX < 0){
                mobilePlatforms[i].setXSpeed(-mobilePlatforms[i].getXSpeed());
            }
            mobilePlatforms[i].setPosition(newX, newY);
        }
        for (int i = 0; i < jumpers.length; i++){
            double newX = jumpers[i].getX()+(tick.getMillis() * jumpers[i].getXSpeed());
            double newY = jumpers[i].getY()+(tick.getMillis() * jumpers[i].getYSpeed());
            if (jumpers[i].getYSpeed() < 0){
                jumpers[i].setYSpeed(0.995*jumpers[i].getYSpeed());
            } else if (jumpers[i].getYSpeed() > 0 && jumperStates[i] == JumperState.FALL){
                jumpers[i].setYSpeed(1.005*jumpers[i].getYSpeed());
            }
            if (newX > EntityFactory.WorldWidth){
                newX = -jumpers[i].getWidth();
            } else if (newX < -jumpers[i].getWidth()){
                newX = EntityFactory.WorldWidth;
            }
            jumpers[i].setPosition(newX, newY);
            for (ImmobileRectangle p : immobilePlatforms){
                if (p.collide(jumpers[i])){
                    p.moveOut(jumpers[i]);
                    if (jumpers[i].getY() < p.getY()){
                        jumperStates[i] = JumperState.SIT;
                    }
                }
            }
            for (MobileRectangle p : mobilePlatforms){
                if (p.collide(jumpers[i])){
                    p.moveOut(jumpers[i]);
                    if (jumpers[i].getY() < p.getY()){
                        jumperStates[i] = JumperState.SIT;
                    }
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void packageInanimateEntities() {
        inanimates = new Entity[immobilePlatforms.length + mobilePlatforms.length + 1];
        for (int i = 0; i < mobilePlatforms.length; i++){
            inanimates[i] = mobilePlatforms[i];
        }
        for (int i = mobilePlatforms.length; i < immobilePlatforms.length + mobilePlatforms.length; i++){
            inanimates[i] = immobilePlatforms[i - mobilePlatforms.length];
        }
        inanimates[inanimates.length-1] = endToken;
    }

    /**
     * Puts a player in the JUMP state.
     * @param index the player number (0 indexed)
     */
    private void startJump(int index){
        jumperStates[index] = JumperState.JUMP;
        jumpers[index].setYSpeed(-FallSpeed);
        jumpTimers[index] = new Timer();
        jumpTimers[index].schedule(new JumpCancel(index), 800);
    }

    /**
     * Takes a player out of the JUMP state
     * @param index the player number (0 indexed)
     */
    private void endJump(int index){
        jumperStates[index] = JumperState.FALL;
        jumpers[index].setYSpeed(FallSpeed);
    }

    // Timer for a player jumping. If the timer runs out, the player's jump ends.
    private class JumpCancel extends TimerTask{
        private int index;
        public JumpCancel(int index){
            this.index = index;
        }
        @Override
        public void run(){
            endJump(index);
        }
    }

    // States for each player which affect their behaviour.
    private enum JumperState{
        JUMP, FALL, SIT;
    }

}
