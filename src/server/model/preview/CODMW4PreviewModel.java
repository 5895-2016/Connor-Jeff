/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.preview;

import java.util.Iterator;
import java.util.Random;
import java.util.Vector;

import io.modelInput.ModelTimerInput;
import server.Server;
import server.model.ShootingCounter;
import server.model.entity.CODMW4Factory;
import server.model.entity.Entity;
import server.model.entity.EntityFactory;
import server.model.entity.ImmobileRectangle;
import server.model.entity.MobileBullet;
import server.model.entity.MobileFighter;

public class CODMW4PreviewModel extends PreviewModel {

	static public double PlayerSpeed = 0.03;
	
	private CODMW4Factory entityFactory;
	private Vector <MobileBullet> bullets;
	private MobileFighter[] fighters;
	private ImmobileRectangle[] barriers;
	private ShootingCounter[] shootingCounter;
	private int shootTick;
	private double[] randomXSpeed;
	private double[] randomYSpeed;
	
	public CODMW4PreviewModel(Server server) {
		super(server);
		entityFactory = new CODMW4Factory();
		fighters = entityFactory.createPlayers();
		players = fighters;
		bullets = new Vector <MobileBullet>();
		barriers = entityFactory.createBarriers();
		shootingCounter = new ShootingCounter[4];
		shootTick = 0;
		randomXSpeed = new double[4];
		randomYSpeed = new double[4];
		
		for (int i = 0; i < 4; i++){
			int random = randomNum(1,5);
			System.out.println("" + random);
			if (random == 1){
				randomXSpeed[i] = PlayerSpeed;
				randomYSpeed[i] = 0;
			} else if (random == 2){
				randomXSpeed[i] = -PlayerSpeed;
				randomYSpeed[i] = PlayerSpeed;
			}else if (random == 3){
				randomXSpeed[i] = 0;
				randomYSpeed[i] = -PlayerSpeed;
			}
			
			shootingCounter[i] = new ShootingCounter();
		}
		clock.start();
		
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void modelTimerUpdate(ModelTimerInput tick) {
		shootTick++;
		
		for (int i = 0; i< 4; i++){
			MobileFighter r = fighters[i];
			double newX = r.getX()+(tick.getMillis() * randomXSpeed[i]);
            double newY = r.getY()+(tick.getMillis() * randomYSpeed[i]);
			
            if (newY > EntityFactory.WorldHeight-r.getHeight()){
                newY = EntityFactory.WorldHeight-r.getHeight();
                randomYSpeed[i] = -randomYSpeed[i];
                randomXSpeed[i] = getRandomSpeed();
            } else if (newY < 0){
                newY = 0;
                randomYSpeed[i] = -randomYSpeed[i];
                randomXSpeed[i] = getRandomSpeed();
            }
            if (newX > EntityFactory.WorldWidth-r.getWidth()){
                newX = EntityFactory.WorldWidth-r.getWidth();
                randomXSpeed[i] = -randomXSpeed[i];
                randomYSpeed[i] = getRandomSpeed();
            } else if (newX < 0){
                newX = 0;
                randomXSpeed[i] = -randomXSpeed[i];
                randomYSpeed[i] = getRandomSpeed();
            }
			
            r.setPosition(newX, newY);
            for (ImmobileRectangle barrier : barriers){
                if (barrier.collide(r)){
                    barrier.moveOut(r);
                    randomXSpeed[i] = -randomXSpeed[i];
                    randomYSpeed[i] = -randomYSpeed[i];
                }
            }
        }
		
		if (shootTick % 25 == 0){
			int randomPlayer = randomNum(1,6);
			bullets.add(entityFactory.addBullet(fighters[randomPlayer-1].getX(), fighters[randomPlayer-1].getY(), (double)randomNum(0,100), (double)randomNum(0,100),randomPlayer));
		}
		
		Iterator<MobileBullet> bulletIterator = bullets.iterator();
        while (bulletIterator.hasNext()){
        	MobileBullet b = bulletIterator.next();
            double newX = b.getX()+(tick.getMillis() * b.getXSpeed());
            double newY = b.getY()+(tick.getMillis() * b.getYSpeed());
            b.setPosition(newX, newY);
            for (ImmobileRectangle w : barriers){
                if (w.collide(b)){
                    bulletIterator.remove();
                    break;
                }
            }    
        }

	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void packageInanimateEntities() {
		inanimates = new Entity[barriers.length + bullets.size()];
		for (int i = 0; i < barriers.length; i++){
			inanimates[i] = barriers[i];
		}
		for (int i = 0; i < bullets.size(); i++){
			inanimates[i+barriers.length] = bullets.elementAt(i);
		}

	}
	
	/**
	 * Returns a constant speed that is either positive, negative, or zero.
	 * @return a constant speed that is either positive, negative, or zero.
	 */
	private double getRandomSpeed(){
		int random = randomNum(1,5);
		double speed = PlayerSpeed;
		if (random == 1){
			speed = PlayerSpeed;
		} else if (random == 2){
			speed = -PlayerSpeed;
		}else if (random == 3){
			speed = 0;
		}
		return speed;
	}
	
	/**
	 * Returns a random integer within a given range.
	 * @param min Minimum value of range
	 * @param max Maximum value of range
	 * @return a random integer within a given range
	 */
	private int randomNum(int min, int max){
		Random rand = new Random();
		return rand.nextInt((max-min)-1) + min;
	}

}
