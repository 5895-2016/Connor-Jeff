/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.entity;

import client.view.animation.Crosshair;
import client.view.animation.Drawable;


public class MobileCrosshair implements Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5088805882033939899L;
	private static int crosshairWidth = 15;
	
	private double x;
    private double y;
    Crosshair crosshair;
	
	public MobileCrosshair(int playerNumber){
		this.x = 100 + (playerNumber - 1)*100;
		this.y = 250;
		crosshair = new Crosshair(playerNumber);
	}
	
	@Override
	public double getX() {
		return x;
	}

	@Override
	public double getY() {
		return y;
	}
	
	public void setPosition(double x, double y){
        this.x = x-7.5;
        this.y = y-5;
    }

	@Override
	public double getWidth() {
		return crosshairWidth;
	}

	@Override
	public double getHeight() {
		return crosshairWidth;
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public Drawable createDrawable(int width, int height) {
		return crosshair;
	}

}
