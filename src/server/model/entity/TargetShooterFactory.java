/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.entity;

public class TargetShooterFactory extends EntityFactory {

	public TargetShooterFactory(){};
	
	/**
     * {@inheritDoc}
     */
	@Override
	public MobileCrosshair[] createPlayers() {
		MobileCrosshair[] crosshairs = new MobileCrosshair[4];
		for(int i = 0; i < 4; i++){
			crosshairs[i] = new MobileCrosshair(i+1);
			crosshairs[i].setPosition(20 + (i*20), 50);
		}
		return crosshairs;
	}
	
	/**
	 * Add a new {@code ImmobileTarget} to a given location with a given size.
	 * @param x X coordinate of location
	 * @param y Y coordinate of location
	 * @param width
	 * @param height
	 * @return a new {@code ImmobileTarget} to a given location with a given size.
	 */
	public ImmobileTarget addTarget(int x, int y, int width, int height){
		return new ImmobileTarget(x,y,width,height);
	}

}
