/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.entity;

import java.io.Serializable;

import client.view.animation.Drawable;

/**
 * An entity that exists in the game world. 
 * Entities model all interactions between actors in the game world.
 */
public interface Entity extends Serializable {
    
    public double getX();
    
    public double getY();

    public double getWidth();
    
    public double getHeight();
    
    /**
     * Creates a {@code Drawable} object that represents this entity.
     * @pre width > 0 && height > 0
     * @param width The drawable's width in pixels.
     * @param height The drawable's height in pixels.
     * @param c The drawable's colour.
     * @return A drawable that represents this entity.
     */
    public Drawable createDrawable(int width, int height);
}
