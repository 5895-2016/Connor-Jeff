/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.entity;

import java.awt.Color;

import client.view.animation.Drawable;
import client.view.animation.Rectangle;

public class MobileFighter extends MobileRectangle {

	private static final long serialVersionUID = 5654349514882278173L;
	private Color color;

	public MobileFighter(double width, double height, Color c){
		super(width, height, c);
		color = c;
	}
	
	public void setColor(Color c){
		color = c;
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public Drawable createDrawable(int width, int height){
        return new Rectangle(width, height, color);
    }
	
}
