/* <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.entity;

import client.view.View;

public class MobileBullet extends MobileCircle {
    
    private static final long serialVersionUID = 222221L;

    private int playerNum;
    
    /**
     * @param diameter The circle's diameter in game space.
     * @param playerNum the player that will shoot the bullet
     */
    public MobileBullet(double diameter, int playerNum){
    	super(diameter, View.playerColors[playerNum-1]);
        this.playerNum = playerNum;
    }
    
    public int getPlayerNum(){
    	return playerNum;
    }

}
