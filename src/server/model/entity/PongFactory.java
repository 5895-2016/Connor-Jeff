/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.entity;

import java.awt.Color;
import java.util.Vector;

import client.view.View;

public class PongFactory extends EntityFactory {
    
    static public int PaddleWidth = 5;
    static public int PaddleLength = 20;
    static public int BallSize = 5;
    static public double BallSpeed = 0.04;
    
    public PongFactory(){
    }
    
    /**
     *{@inheritDoc}
     */
    public MobileRectangle[] createPlayers(){
        MobileRectangle[] paddles = new MobileRectangle[4];
        paddles[0] = new MobileRectangle(PaddleWidth, PaddleLength, View.playerColors[0]);
        paddles[1] = new MobileRectangle(PaddleLength, PaddleWidth, View.playerColors[1]);
        paddles[2] = new MobileRectangle(PaddleWidth, PaddleLength, View.playerColors[2]);
        paddles[3] = new MobileRectangle(PaddleLength, PaddleWidth, View.playerColors[3]);
        paddles[0].setPosition(0, 
                        (WorldHeight/2) - (PaddleLength/2));
        paddles[1].setPosition((WorldWidth/2) - (PaddleLength/2),
                         0);
        paddles[2].setPosition(WorldWidth-PaddleWidth,
                        (WorldHeight/2) - (PaddleLength/2));
        paddles[3].setPosition((WorldWidth/2) - (PaddleLength/2),
                         WorldWidth-PaddleWidth);
        
        return paddles;
    }
    
    /**
     * Creates an {@code ImmobileRectangle} at each corner of the game world.
     * @return an array of 4 rectangles, one at each corner.
     */
    public ImmobileRectangle[] createCorners(){
        ImmobileRectangle[] corners = new ImmobileRectangle[4];
        for (int i = 0; i < 4; i++){
            corners[i] = new ImmobileRectangle(PaddleWidth, PaddleWidth, Color.BLACK);
        }
        corners[0].setPosition(0, 0);
        corners[1].setPosition(WorldWidth-PaddleWidth, 0);
        corners[2].setPosition(0, WorldWidth-PaddleWidth);
        corners[3].setPosition(WorldWidth-PaddleWidth, WorldWidth-PaddleWidth);
        
        return corners;
    }
    
    /**
     * Adds a new {@code MobileCircle} which starts at the center of the game world and has a random direction.
     * @param balls a vector of balls
     */
    public void addBall(Vector<MobileCircle> balls){
        balls.add(new MobileCircle(BallSize, Color.BLACK));
        balls.lastElement().setPosition(WorldWidth/2 - BallSize/2, WorldHeight/2 - BallSize/2);
        double angle = Math.random() * 360;
        balls.lastElement().setXSpeed(BallSpeed * Math.cos(angle));
        balls.lastElement().setYSpeed(BallSpeed * Math.sin(angle));
    }

}
