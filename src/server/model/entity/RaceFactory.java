/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.entity;

import java.awt.Color;

import client.view.View;

public class RaceFactory extends EntityFactory {
    
    public static double EndDistance = EntityFactory.WorldWidth*10;

    /**
     * {@inheritDoc}
     */
    @Override
    public MobileRectangle[] createPlayers() {
        MobileRectangle[] rectangles = new MobileRectangle[4];
        for (int i = 0; i < 4; i++){
            rectangles[i] = new MobileRectangle(5, 5, View.playerColors[i]);
            rectangles[i].setXSpeed(0.1);
            rectangles[i].setPosition(0, 20*i + 10);
        }
        return rectangles;
    }
    
    /**
     * Creates the walls scattered through the race in their initial states.
     * @return an array of rectangles representing the walls in their initial states.
     */
    public ImmobileRectangle[] createWalls(){
        ImmobileRectangle[] walls = new ImmobileRectangle[(int) EndDistance/(EntityFactory.WorldWidth/2)];
        for (int i = 0; i < walls.length/2; i++){
            walls[2*i] = new ImmobileRectangle(5, 35, Color.BLACK);
            walls[2*i + 1] = new ImmobileRectangle(5, 15, Color.BLACK);
            walls[2*i].setPosition(30 + (EntityFactory.WorldWidth*i),
                                   (Math.random()*(EntityFactory.WorldHeight+walls[0].getHeight()/2))-(walls[0].getHeight()/2));
            walls[2*i + 1].setPosition(80 + (EntityFactory.WorldWidth*i),
                                       (Math.random()*(EntityFactory.WorldHeight+walls[1].getHeight()/2))-(walls[1].getHeight()/2));
        }
        return walls;
    }
    
    /**
     * Creates the finish line in its initial state.
     * @return an array of rectangles representing the finish line in its initial state.
     */
    public ImmobileRectangle[] createFinishLine(){
        ImmobileRectangle[] finishLine = new ImmobileRectangle[25];
        for (int i = 0; i < finishLine.length; i++){
            finishLine[i] = new ImmobileRectangle(4, 4, Color.BLACK);
            finishLine[i].setPosition(EndDistance +(4 * (i%2)), i * 4);
        }
        return finishLine;
    }
    
    /**
     * Creates rectangles representing the progress bar at the top of the screen.</br>
     * The first rectangle is the grey background behind the markers and the next 4 rectangles are the markers for each player.
     * @return an array containing the markers and the background rectangle.
     */
    public MobileRectangle[] createMarkers(){
        MobileRectangle[] markers = new MobileRectangle[5];
        markers[0] = new MobileRectangle(EntityFactory.WorldWidth, 5, Color.DARK_GRAY);
        markers[0].setPosition(0, 0);
        for (int i = 1; i < markers.length; i++){
            markers[i] = new MobileRectangle(1, 5, View.playerColors[i-1]);
            markers[i].setPosition(0, 0);
        }
        return markers;
    }

}
