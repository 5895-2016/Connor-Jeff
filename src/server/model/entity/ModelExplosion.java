/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.entity;

import client.view.animation.Drawable;
import client.view.animation.Explosion;

public class ModelExplosion implements Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1297296588138625769L;
	private int x, y, width, height;
	private Explosion explosion;
	
	public ModelExplosion(int x, int y){
		this.x = x;
		this.y = y;
		explosion = new Explosion((int)getX(),(int)getY());
	}
	
	@Override
	public double getX() {
		return x-5;
	}

	@Override
	public double getY() {
		return y-2.5;
	}

	@Override
	public double getWidth() {
		return width;
	}

	@Override
	public double getHeight() {
		return height;
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public Drawable createDrawable(int width, int height) {
		return explosion;
	}

}
