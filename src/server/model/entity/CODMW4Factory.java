/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.entity;

import java.awt.Color;

import client.view.View;

public class CODMW4Factory extends EntityFactory {

	static public int FighterWidth = 5;
    static public int FighterLength = 5;
    static public int BarrierWidth = 5;
    static public int BarrierLength = 20;
    static public int BulletSize = 2;
    static public double BulletSpeed = 0.07;
	
	public CODMW4Factory(){
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public MobileFighter[] createPlayers() {
		MobileFighter[] players = new MobileFighter[4];
		for (int i = 0; i < 4; i++){
			players[i] = new MobileFighter(FighterWidth,FighterLength,View.playerColors[i]);
		}
		players[0].setPosition(5, 5);
		players[1].setPosition(90, 5);
		players[2].setPosition(5, 90);
		players[3].setPosition(90, 90);
		
		return players;
	}
	
	/**
	 * Creates barriers with set locations and sizes
	 * @return An array of barriers with set locations and sizes
	 */
	public ImmobileRectangle[] createBarriers() {
		ImmobileRectangle[] barriers = new ImmobileRectangle[8];
		for (int i = 0; i < 4; i++){
			barriers[i] = new ImmobileRectangle(BarrierLength,BarrierWidth, Color.BLACK);
		}
		
		barriers[0].setPosition(15, 15);
		barriers[1].setPosition(65, 15);
		barriers[2].setPosition(15, 80);
		barriers[3].setPosition(65, 80);
		
		for (int i = 4; i < 8; i++){
			barriers[i] = new ImmobileRectangle(BarrierWidth,BarrierLength, Color.BLACK);
		}
		
		barriers[4].setPosition(15, 15);
		barriers[5].setPosition(80, 15);
		barriers[6].setPosition(15, 65);
		barriers[7].setPosition(80, 65);
		
		return barriers;
	}

	/**
	 * Creates a bullet that starts at the given players location and is directed at a given clicked location.
	 * @param playerX Player's X coordinate
	 * @param playerY Player's Y coordinate
	 * @param clickedX X coordinate where user clicked
	 * @param clickedY Y coordinate where user clicked
	 * @param playerNum The player that clicked
	 * @return A bullet with set location and direction
	 */
	public MobileBullet addBullet(double playerX, double playerY, double clickedX, double clickedY, int playerNum){
		double height = clickedY-playerY;
		double length = clickedX-playerX;
		double angle = Math.atan(Math.abs(height/length));
		double xSpeed = BulletSpeed;
		double ySpeed = BulletSpeed;
		if (height < 0){
			ySpeed = -BulletSpeed;
		}
		if (length < 0){
			xSpeed = -BulletSpeed;
		}
		MobileBullet bullet = new MobileBullet(BulletSize, playerNum);
		bullet.setPosition(playerX, playerY);
		bullet.setXSpeed(xSpeed * Math.cos(angle));
		bullet.setYSpeed(ySpeed * Math.sin(angle));
		
		return bullet;
	}
	
}
