/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.entity;

public abstract class EntityFactory {
    
    // constants for use in each game model
    static public int WorldWidth = 100;
    static public int WorldHeight = 100;
    
    public EntityFactory(){
    }
    
    /**
     * Creates entities representing each player.</br>
     * Each subclass knows the type and initial state of the players for a specific minigame.
     * @return an array containing entities representing each player in their initial state.
     */
    public abstract Entity[] createPlayers();

}
