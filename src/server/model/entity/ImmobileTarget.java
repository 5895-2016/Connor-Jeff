/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.entity;


import client.view.animation.Drawable;
import client.view.animation.Target;

public class ImmobileTarget implements Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4553956354494027121L;
	private int x;
    private int y;
    private int width;
    private int height;
    private Target target;
	
	public ImmobileTarget(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		target = new Target(width*5,height*5);
	}
	
	@Override
	public double getX() {
		return x;
	}

	@Override
	public double getY() {
		return y;
	}

	@Override
	public double getWidth() {
		return width;
	}

	@Override
	public double getHeight() {
		return height;
	}

	/**
	 * Determines if the user clicked within the boundaries of the target.
	 * @param xCoord X coordinate where user clicked.
	 * @param yCoord Y coordinate where user clicked.
	 * @return A boolean indicating if the user clicked on the target.
	 */
	public boolean wasIShot(double xCoord, double yCoord){
		if (xCoord/5 > getX() && xCoord/5 < (getX() + getWidth())){
			return (yCoord/5 > getY() && yCoord/5 < (getY() + getHeight()));
		}
		else {
			return false;
		}
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public Drawable createDrawable(int width, int height) {
		return target;
	}

}
