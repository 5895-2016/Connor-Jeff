/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.entity;

import java.awt.Color;

import client.view.animation.Drawable;
import client.view.animation.Ellipse;

public class MobileCircle implements Entity {
    
    private static final long serialVersionUID = 222222L;

    private double x;
    private double y;
    private double centerX;
    private double centerY;
    private double diameter;
    private double xSpeed;
    private double ySpeed;
    private Color color;
    
    /**
     * @pre diameter > 0
     * @param diameter The circle's diameter in game space.
     * @param color the circle's colour.
     */
    public MobileCircle(double diameter, Color color){
        this.diameter = diameter;
        xSpeed = 0;
        ySpeed = 0;
        this.color = color;
    }
    
    public void setPosition(double x, double y){
        this.x = x;
        this.y = y;
        this.centerX = x + (diameter/2);
        this.centerY = y + (diameter/2);
    }
    
    public void setXSpeed(double xSpeed){
        this.xSpeed = xSpeed;
    }
    
    public void setYSpeed(double ySpeed){
        this.ySpeed = ySpeed;
    }
    
    public double getXSpeed(){
        return xSpeed;
    }
    
    public double getYSpeed(){
        return ySpeed;
    }
    
    public double getXCenter(){
        return centerX;
    }
    
    public double getYCenter(){
        return centerY;
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getWidth() {
        return diameter;
    }

    @Override
    public double getHeight() {
        return diameter;
    }
    
    /**
     * Detects if a collision occurred between the two entities.
     * @param r Another entity.
     * @return true if the entities overlap (collision).
     */
    public boolean collide(MobileRectangle r){
        if (centerY > r.getY() && centerY < r.getY()+r.getHeight()){
            if (this.getX()+this.getWidth() > r.getX() && this.getX() < r.getX()+r.getWidth()){
                return true;
            }
        }
        else if (centerX > r.getX() && centerX < r.getX()+r.getWidth()){
            if (this.getY()+this.getHeight() > r.getY() && this.getY() < r.getY()+r.getHeight()){
                return true;
            }
        }
        else{
            double cornerX;
            double cornerY;
            if (centerX < r.getX() && centerY < r.getY()){
                cornerX = r.getX();
                cornerY = r.getY();
            } else if (centerX < r.getX() && centerY > r.getY()+r.getHeight()){
                cornerX = r.getX();
                cornerY = r.getY()+r.getHeight();
            } else if (centerX > r.getX()+r.getWidth() && centerY < r.getY()){
                cornerX = r.getX()+r.getWidth();
                cornerY = r.getY();
            } else{
                cornerX = r.getX()+r.getWidth();
                cornerY = r.getY()+r.getHeight();
            }
            
            double distance = Math.sqrt(Math.pow(centerX-cornerX, 2) + Math.pow(centerY-cornerY, 2));
            if (distance < diameter/2){
                return true;
            }
        }
        return false;
    }
    
    /**
     * {@inheritDoc}
     */
    public Drawable createDrawable(int width, int height){
        // A perfect circle in game space may not be a perfect circle on the screen
        return new Ellipse(width, height, color);
    }

}
