/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */
package server.model.entity;

import java.awt.Color;

import client.view.animation.Drawable;
import client.view.animation.Rectangle;

public class MobileRectangle implements Entity {
    
    private static final long serialVersionUID = 333333L;
    
    private double x;
    private double y;
    private double width;
    private double height;
    private double xSpeed;
    private double ySpeed;
    private Color color;
    
    /**
     * @pre width > 0 && height > 0
     * @param width The width in game space.
     * @param height The height in game space.
     * @param color the rectangle's colour.
     */
    public MobileRectangle(double width, double height, Color color){
        this.width = width;
        this.height = height;
        xSpeed = 0;
        ySpeed = 0;
        this.color = color;
    }
    
    public void setPosition(double x, double y){
        this.x = x;
        this.y = y;
    }
    
    public void setXSpeed(double xSpeed){
        this.xSpeed = xSpeed;
    }
    
    public void setYSpeed(double ySpeed){
        this.ySpeed = ySpeed;
    }
    
    public double getXSpeed(){
        return xSpeed;
    }
    
    public double getYSpeed(){
        return ySpeed;
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getWidth() {
        return width;
    }

    @Override
    public double getHeight() {
        return height;
    }
    
    /**
     * Detects if a collision occurred between the two entities.
     * @param r Another entity.
     * @return true if the entities overlap (collision).
     */
    public boolean collide(MobileRectangle r){
        if (this.getX() < (r.getX()+r.getWidth()) &&
            this.getY() < (r.getY()+r.getHeight()) &&
            r.getX() < (this.getX()+this.getWidth()) &&
            r.getY() < (this.getY()+this.getHeight())){
            return true;
        }
        return false;
    }
    
    /**
     * Moves a {@code MobileRectangle} out of this rectangle, taking the shortest distance to move it out.
     * @pre r is overlapping this object (this.collide(r) == true).
     * @param r The overlapping rectangle.
     */
    public void moveOut(MobileRectangle r){
        double deltaX = 1000;
        double deltaY = 1000;
        if (r.getX() < this.getX()+this.getWidth() && r.getX() > (this.getX()+this.getWidth()-r.getWidth())){
            deltaX = (this.getX()+this.getWidth())-r.getX();
        } else if ((r.getX()+r.getWidth()) > this.getX() && 
                   (r.getX()+r.getWidth()) < (this.getX()+r.getWidth())){
            deltaX = (this.getX()-r.getWidth())-r.getX();
        }
        if (r.getY() < this.getY()+this.getHeight() && r.getY() > (this.getY()+this.getHeight()-r.getHeight())){
            deltaY = (this.getY()+this.getHeight())-r.getY();
        } else if ((r.getY()+r.getHeight()) > this.getY() && 
                   (r.getY()+r.getHeight()) < (this.getY()+r.getHeight())){
            deltaY = (this.getY()-r.getHeight())-r.getY();
        }
        if (Math.abs(deltaX) <= Math.abs(deltaY)){
            r.setPosition(r.getX()+deltaX, r.getY());
        } else if (Math.abs(deltaY) <= Math.abs(deltaX)){
            r.setPosition(r.getX(), r.getY()+deltaY);
        }
    }
    
    /**
     * Detects if a collision occurred between the two entities.
     * @param c Another entity.
     * @return true if the entities overlap (collision).
     */
    public boolean collide(MobileCircle c){
        if (c.getYCenter() >= this.getY() && c.getYCenter() <= this.getY()+this.getHeight()){
            if (c.getX()+c.getWidth() >= this.getX() && c.getX() <= this.getX()+this.getWidth()){
                return true;
            }
        }
        else if (c.getXCenter() >= this.getX() && c.getXCenter() <= this.getX()+this.getWidth()){
            if (c.getY()+c.getHeight() >= this.getY() && c.getY() <= this.getY()+this.getHeight()){
                return true;
            }
        }
        else{
            double cornerX;
            double cornerY;
            if (c.getXCenter() <= this.getX() && c.getYCenter() <= this.getY()){
                cornerX = this.getX();
                cornerY = this.getY();
            } else if (c.getXCenter() <= this.getX() && c.getYCenter() >= this.getY()+this.getHeight()){
                cornerX = this.getX();
                cornerY = this.getY()+this.getHeight();
            } else if (c.getXCenter() >= this.getX()+this.getWidth() && c.getYCenter() <= this.getY()){
                cornerX = this.getX()+this.getWidth();
                cornerY = this.getY();
            } else{
                cornerX = this.getX()+this.getWidth();
                cornerY = this.getY()+this.getHeight();
            }
            
            double distance = Math.sqrt(Math.pow(c.getXCenter()-cornerX, 2) + Math.pow(c.getYCenter()-cornerY, 2));
            if (distance <= c.getWidth()/2){
                return true;
            }
        }
        return false;
    }
    
    /**
     * Deflects a {@code MobileCircle} that has collided with this object.
     * @pre c is overlapping this object (this.collide(c) == true).
     * @param c The circle that has collided.
     */
    public void deflect(MobileCircle c){
        // Circle hits the top or botton of rectangle
        if (c.getYCenter() >= this.getY() && c.getYCenter() <= this.getY()+this.getHeight()){
            if (c.getX() <= this.getX()){
                c.setPosition(this.getX()-c.getWidth(), c.getY());
                c.setXSpeed(-c.getXSpeed());
            } else if (c.getX() >= this.getX()){
                c.setPosition(this.getX()+this.getWidth(), c.getY());
                c.setXSpeed(-c.getXSpeed());
            }
        }
        // Circle hits a flat sid of rectangle
        else if (c.getXCenter() >= this.getX() && c.getXCenter() <= this.getX()+this.getWidth()){
            if (c.getY() <= this.getY()){
                c.setPosition(c.getX(), this.getY()-c.getHeight());
                c.setYSpeed(-c.getYSpeed());
            } else if (c.getY() >= this.getY()){
                c.setPosition(c.getX(), this.getY()+this.getHeight());
                c.setYSpeed(-c.getYSpeed());
            }
        }
        // Circle hits a corner of the rectangle
        else{
            double cornerX;
            double cornerY;
            if (c.getXCenter() <= this.getX() && c.getYCenter() <= this.getY()){
                cornerX = this.getX();
                cornerY = this.getY();
            } else if (c.getXCenter() <= this.getX() && c.getYCenter() >= this.getY()+this.getHeight()){
                cornerX = this.getX();
                cornerY = this.getY()+this.getHeight();
            } else if (c.getXCenter() >= this.getX()+this.getWidth() && c.getYCenter() <= this.getY()){
                cornerX = this.getX()+this.getWidth();
                cornerY = this.getY();
            } else{
                cornerX = this.getX()+this.getWidth();
                cornerY = this.getY()+this.getHeight();
            }
            double deltax = c.getXCenter()-cornerX;
            double deltay = c.getYCenter()-cornerY;
            double distance = Math.sqrt(Math.pow(deltax, 2) + Math.pow(deltay, 2));
            double newDeltax = (c.getWidth()/2) * (deltax/distance);
            double newDeltay = (c.getHeight()/2) * (deltay/distance);
            double pScalar = (c.getXSpeed()*newDeltax + c.getYSpeed()*newDeltay)/(c.getWidth()/2);
            double px = pScalar * newDeltax / (c.getWidth()/2);
            double py = pScalar * newDeltay / (c.getHeight()/2);
            
            c.setPosition(cornerX + newDeltax - c.getWidth()/2, cornerY + newDeltay - c.getHeight()/2);
            c.setXSpeed(c.getXSpeed() - (2 * px));
            c.setYSpeed(c.getYSpeed() - (2 * py));
        }
    }
    
    /**
     * {@inheritDoc}
     */
    public Drawable createDrawable(int width, int height){
        return new Rectangle(width, height, color);
    }

}
