/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.entity;

import java.awt.Color;

import client.view.View;

public class JumpFactory extends EntityFactory {

    public static double PlatformSpeed = 0.02;
    public static int mobilePlatformCount = 20;

    /**
     * {@inheritDoc}
     */
    @Override
    public MobileRectangle[] createPlayers() {
        MobileRectangle[] jumpers = new MobileRectangle[4];
        for (int i = 0; i < 4; i++){
            jumpers[i] = new MobileRectangle(5, 5, View.playerColors[i]);
            jumpers[i].setPosition((EntityFactory.WorldWidth/4)*i + EntityFactory.WorldWidth/8, EntityFactory.WorldHeight-10);
        }
        return jumpers;
    }
    
    /**
     * Creates the immobile platforms for the game.</br>
     * There is currently only one immobile rectangle representing the floor but more can be added to the game by changing the size of the array.
     * @return an array holding the immobile platforms in their initial states.
     */
    public ImmobileRectangle[] createImmobilePlatforms(){
        ImmobileRectangle[] immobilePlatforms = new ImmobileRectangle[1];
        immobilePlatforms[0] = new ImmobileRectangle(EntityFactory.WorldWidth+10, 5, Color.BLACK);
        immobilePlatforms[0].setPosition(-5, EntityFactory.WorldHeight-5);
        for (int i = 1; i < immobilePlatforms.length; i++){
            immobilePlatforms[i] = new ImmobileRectangle(20, 5, Color.BLACK);
            immobilePlatforms[i].setPosition(16 * ((2*i)%5), -50*(i-1) + (Math.random()*20-10));
        }
        return immobilePlatforms;
    }

    /**
     * Creates the mobile platforms for the game.
     * @return an array holding the mobile platforms in their initial states.
     */
    public MobileRectangle[] createMobilePlatforms(){
        MobileRectangle[] mobilePlatforms = new MobileRectangle[mobilePlatformCount];
        for (int i = 0; i < mobilePlatforms.length/2; i++){
            mobilePlatforms[i] = new MobileRectangle(Math.random()*20 + 20, 3, Color.GRAY);
            mobilePlatforms[i].setPosition(Math.random()*(EntityFactory.WorldWidth-mobilePlatforms[i].getWidth()), -25*(i-3));
            mobilePlatforms[i].setXSpeed(PlatformSpeed);
        }
        for (int i = mobilePlatforms.length/2; i < mobilePlatforms.length; i++){
            mobilePlatforms[i] = new MobileRectangle(Math.random()*10 + 10, 3, Color.GRAY);
            mobilePlatforms[i].setPosition(Math.random()*(EntityFactory.WorldWidth-mobilePlatforms[i].getWidth()), -25*(i-3));
            mobilePlatforms[i].setXSpeed(1.2*PlatformSpeed);
        } 
        return mobilePlatforms;
    }
    
    /**
     * Creates the yellow token at the end of the game.
     * @return The end token in its initial state.
     */
    public ImmobileRectangle createEndToken(){
        ImmobileRectangle endToken = new ImmobileRectangle(7, 7, Color.YELLOW);
        endToken.setPosition(EntityFactory.WorldWidth/2 - 5,
                             -25*(mobilePlatformCount-4)-50);
        return endToken;
    }
    

}
