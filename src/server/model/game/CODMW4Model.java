/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.game;

import java.util.Iterator;
import java.util.Vector;

import io.modelInput.GameInput;
import io.modelInput.LobbyInput;
import io.modelInput.ModelTimerInput;
import server.Server;
import server.model.PlayerModel;
import server.model.ShootingCounter;
import server.model.entity.CODMW4Factory;
import server.model.entity.Entity;
import server.model.entity.EntityFactory;
import server.model.entity.ImmobileRectangle;
import server.model.entity.MobileBullet;
import server.model.entity.MobileFighter;

public class CODMW4Model extends GameModel {

	static public double PlayerSpeed = 0.05;
	
	private CODMW4Factory entityFactory;
	private Vector <MobileBullet> bullets;
	private MobileFighter[] fighters;
	private ImmobileRectangle[] barriers;
	private ShootingCounter[] shootingCounter;
	private ShootingCounter[] hitCounter;
	private boolean[] shooterReady;
	private boolean[] shooterHit;
	private Vector <Integer> playersShooting;
	private int[] lives;
	private double[] playerXPos;
	private double[] playerYPos;
	private GameOverScore gameOverChecker;
	
	public CODMW4Model(Server server) {
		super(server);
		entityFactory = new CODMW4Factory();
		fighters = entityFactory.createPlayers();
		players = fighters;
		bullets = new Vector <MobileBullet>();
		barriers = entityFactory.createBarriers();
		shooterReady = new boolean[]{true,true,true,true};
		shooterHit = new boolean[]{false,false,false,false};
		playersShooting = new Vector <Integer>();
		shootingCounter = new ShootingCounter[4];
		hitCounter = new ShootingCounter[4];
		lives = new int[]{3,3,3,3};
		scores = lives;
		
		playerXPos = new double[4];
		playerYPos = new double[4];
		
		gameOverChecker = new GameOverScore(this, 3);
		
		for (int i = 0; i < 4; i++){
			shootingCounter[i] = new ShootingCounter();
			hitCounter[i] = new ShootingCounter();
		}
		clock.start();
		
	}

	/**
     * {@inheritDoc}
     */
	@Override
	synchronized public void gameUpdate(GameInput gameInput) {
		int playerNum = gameInput.getPlayerNumber();
		MobileFighter r = fighters[playerNum - 1];
		if (r.getYSpeed() == 0){
            if (gameInput.up()){
                r.setYSpeed(-PlayerSpeed);
            } else if (gameInput.down()){
                r.setYSpeed(PlayerSpeed);
            } else{
                r.setYSpeed(0);
            }
		}else if (r.getYSpeed() == PlayerSpeed){
            if (!gameInput.down()){
                r.setYSpeed(0);
            } else if (gameInput.up()){
                r.setYSpeed(-PlayerSpeed);
            } else{
                r.setYSpeed(PlayerSpeed);
            }
        } else if (r.getYSpeed() == -PlayerSpeed){
            if (!gameInput.up()){
                r.setYSpeed(0);
            } else if (gameInput.down()){
                r.setYSpeed(PlayerSpeed);
            }else{
                r.setYSpeed(-PlayerSpeed);
            }
        } 
        if (r.getXSpeed() == 0){
            if (gameInput.left()){
                r.setXSpeed(-PlayerSpeed);
            } else if (gameInput.right()){
                r.setXSpeed(PlayerSpeed);
            } else{
                r.setXSpeed(0);
            }
        } else if (r.getXSpeed() == PlayerSpeed){
            if (!gameInput.right()){
                r.setXSpeed(0);
            } else if (gameInput.left()){
                r.setXSpeed(-PlayerSpeed);
            } else{
                r.setXSpeed(PlayerSpeed);
            }
        } else if (r.getXSpeed() == -PlayerSpeed){
            if (!gameInput.left()){
                r.setXSpeed(0);
            } else if (gameInput.right()){
                r.setXSpeed(PlayerSpeed);
            }else{
                r.setXSpeed(-PlayerSpeed);
            }
        }
        
        if (gameInput.getClicked()){
        	if (shooterReady[playerNum-1] && lives[playerNum-1] != 0){
        		bullets.add(entityFactory.addBullet(fighters[playerNum-1].getX(), fighters[playerNum-1].getY(), gameInput.getMouseClickPos().getX()/5, gameInput.getMouseClickPos().getY()/5, playerNum));
        		shooterReady[playerNum-1] = false;
        		playersShooting.add(playerNum);
        	}
        }
    }
	
	/**
     * {@inheritDoc}
     */
	@Override
	synchronized public void modelTimerUpdate(ModelTimerInput tick) {
		for (int i = 0; i< 4; i++){
			MobileFighter r = fighters[i];
			double newX = r.getX()+(tick.getMillis() * r.getXSpeed());
            double newY = r.getY()+(tick.getMillis() * r.getYSpeed());
			if (!shooterHit[i]){
	            if (newY > EntityFactory.WorldHeight-r.getHeight()){
	                newY = EntityFactory.WorldHeight-r.getHeight();
	            } else if (newY < 0){
	                newY = 0;
	            }
	            if (newX > EntityFactory.WorldWidth-r.getWidth()){
	                newX = EntityFactory.WorldWidth-r.getWidth();
	            } else if (newX < 0){
	                newX = 0;
	            }
			}
            r.setPosition(newX, newY);
            for (ImmobileRectangle barrier : barriers){
                if (barrier.collide(r)){
                    barrier.moveOut(r);
                }
            }
        }
		
		Iterator<MobileBullet> bulletIterator = bullets.iterator();
        while (bulletIterator.hasNext()){
        	MobileBullet b = bulletIterator.next();
            double newX = b.getX()+(tick.getMillis() * b.getXSpeed());
            double newY = b.getY()+(tick.getMillis() * b.getYSpeed());
            b.setPosition(newX, newY);
            for (ImmobileRectangle w : barriers){
                if (w.collide(b)){
                    bulletIterator.remove();
                    break;
                }
            }
            for (int i = 0; i < 4; i++){
            	if (lives[i] != 0 && !shooterHit[i]){
	            	if (fighters[i].collide(b) && b.getPlayerNum() != (i + 1)){
	            		shooterHit[i] = true;
	            		lives[i]--;
	            		if (lives[i] == 0){
	            			fighters[i].setPosition(200,200);
	            			gameOverChecker.addScore(i+1);
	            		}
	                    bulletIterator.remove();
	                    break;
	                }
            	}
            }
        }
        
        for (int i = 0; i < 4; i++){
        	if (!shooterReady[i]){
        		shootingCounter[i].increment();
        	}
        	if(shootingCounter[i].getCount() == 75){
            	shooterReady[i] = true;
            	shootingCounter[i].reset();
            }
        	if (lives[i] != 0){
	        	if (shooterHit[i]){
	        		hitCounter[i].increment();
	        	}
	        	
	        	if (hitCounter[i].getCount() % 25 == 0){
	        		playerBlink(i+1);
	        	}
        	}
        }
        
	}
	
	/**
	 * Called when a player has been hit by a MobileBullet. Makes the player model blink three times, every 1/4 second,
	 * to represent they are temporarily immune to more attacks.
	 * @param playerNum - The player that has been hit
	 */
	private void playerBlink(int playerNum){
		if (hitCounter[playerNum-1].getCount() == 25){
			playerXPos[playerNum -1] = fighters[playerNum -1].getX();
			playerYPos[playerNum -1] = fighters[playerNum -1].getY();
			fighters[playerNum -1].setPosition(200, 200);
			//fighters[playerNum -1].setColor(Color.WHITE);
		}
		if (hitCounter[playerNum-1].getCount() == 50){
			fighters[playerNum-1].setPosition(playerXPos[playerNum -1], playerYPos[playerNum -1]);
			//fighters[playerNum -1].setColor(View.playerColors[playerNum-1]);
		}
		if (hitCounter[playerNum-1].getCount() == 75){
			playerXPos[playerNum -1] = fighters[playerNum -1].getX();
			playerYPos[playerNum -1] = fighters[playerNum -1].getY();
			fighters[playerNum -1].setPosition(200, 200);
			//fighters[playerNum -1].setColor(Color.WHITE);
		}
		if (hitCounter[playerNum-1].getCount() == 100){
			fighters[playerNum-1].setPosition(playerXPos[playerNum -1], playerYPos[playerNum -1]);
			//fighters[playerNum -1].setColor(View.playerColors[playerNum-1]);
		}
		if (hitCounter[playerNum-1].getCount() == 125){
			playerXPos[playerNum -1] = fighters[playerNum -1].getX();
			playerYPos[playerNum -1] = fighters[playerNum -1].getY();
			fighters[playerNum -1].setPosition(200, 200);
			//fighters[playerNum -1].setColor(Color.WHITE);
		}
		if (hitCounter[playerNum-1].getCount() == 150){
			fighters[playerNum-1].setPosition(playerXPos[playerNum -1], playerYPos[playerNum -1]);
			//fighters[playerNum -1].setColor(View.playerColors[playerNum-1]);
		}
		if (hitCounter[playerNum-1].getCount() == 175){
			playerXPos[playerNum -1] = fighters[playerNum -1].getX();
			playerYPos[playerNum -1] = fighters[playerNum -1].getY();
			fighters[playerNum -1].setPosition(200, 200);
			//fighters[playerNum -1].setColor(Color.WHITE);
		}
		if (hitCounter[playerNum-1].getCount() == 200){
			fighters[playerNum-1].setPosition(playerXPos[playerNum -1], playerYPos[playerNum -1]);
			//fighters[playerNum -1].setColor(View.playerColors[playerNum-1]);
			hitCounter[playerNum-1].reset();
			shooterHit[playerNum-1] = false;
		}
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void packageInanimateEntities() {
		inanimates = new Entity[barriers.length + bullets.size()];
		for (int i = 0; i < barriers.length; i++){
			inanimates[i] = barriers[i];
		}
		for (int i = 0; i < bullets.size(); i++){
			inanimates[i+barriers.length] = bullets.elementAt(i);
		}
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void packageStaticEntities() {
		statics = new Entity[]{};
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void sortForRanking(PlayerModel[] results){
		for (int i = 0; i < scores.length; i++){
            results[i] = new PlayerModel(scores[i], 0, server.getPlayerName(i+1), i+1);
        }
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
    public void publishToDisplay(PlayerModel[] results){
		for (int i = 0; i < scores.length; i++){
            results[i].setScore(scores[i]);
            if (results[i].getPosition() == 1){
                server.updateLobby(new LobbyInput(100, results[i].getPlayerNumber()));
            }
        }
    }

}
