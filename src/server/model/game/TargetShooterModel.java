/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.game;

import java.util.Random;
import java.util.Vector;

import io.modelInput.GameInput;
import io.modelInput.LobbyInput;
import io.modelInput.ModelTimerInput;
import server.Server;
import server.model.ExplosionCounter;
import server.model.PlayerModel;
import server.model.entity.Entity;
import server.model.entity.EntityFactory;
import server.model.entity.ImmobileTarget;
import server.model.entity.MobileCrosshair;
import server.model.entity.ModelExplosion;
import server.model.entity.TargetShooterFactory;

public class TargetShooterModel extends GameModel{
	
	private TargetShooterFactory entityFactory;
	private MobileCrosshair[] crosshairs;
	private Vector <ImmobileTarget> targets;
	private Vector <ModelExplosion> explosions;
	private Vector <ExplosionCounter> explosionCounters;
	private int playerNumber;
	private int counter;
	private int limit;
	private double nextX[] = {0,0,0,0};
	private double nextY[] = {0,0,0,0};
	private double clickedXCoord, clickedYCoord;
	private boolean clicked;
	private boolean readyForClick;
	
	
	public TargetShooterModel(Server server) {
		super(server);
		entityFactory = new TargetShooterFactory();
		targets = new Vector<ImmobileTarget>();
		explosions = new Vector <ModelExplosion>();
		explosionCounters = new Vector <ExplosionCounter>();
		crosshairs = entityFactory.createPlayers();
		players = crosshairs;
		clicked = false;
		readyForClick = true;
		counter = 0;
		limit = 600;
		
		gameOverChecker = new GameOverTimer(this, 30);
		
		clock.start();
	}
	
	/**
     * {@inheritDoc}
     */
	synchronized public void gameUpdate(GameInput gameInput){
		nextX[gameInput.getPlayerNumber()-1] = (double)(gameInput.getMousePos().x/5);
		nextY[gameInput.getPlayerNumber()-1] = (double)(gameInput.getMousePos().y/5);
		clickedXCoord = gameInput.getMouseClickPos().getX();
		clickedYCoord = gameInput.getMouseClickPos().getY();
		if (readyForClick){
			playerNumber = gameInput.getPlayerNumber();
			clicked = gameInput.getClicked();
			if (clicked == true) {readyForClick = false;}
		}	
	}
	
	/**
     * {@inheritDoc}
     */
	synchronized public void modelTimerUpdate(ModelTimerInput tick){
		counter++;
		if(counter == limit){
			addTarget();
			counter = 0;
			limit = randomNum(50, 300);
		}
		
		if (clicked){
			for (int i = 0; i < targets.size(); i++){
				if (targets.elementAt(i).wasIShot(clickedXCoord, clickedYCoord)){
					targets.removeElementAt(i);
					scores[playerNumber-1]++;
				}
			}
			explosions.add(new ModelExplosion((int)clickedXCoord/5,(int)clickedYCoord/5));
			explosionCounters.add(new ExplosionCounter());
			
			clicked = false;
			readyForClick = true;
		}
		
		if (explosionCounters.size() != 0){
			for (ExplosionCounter c : explosionCounters){
				c.increment();
				if (c.getCount() == 20){
					explosions.remove(0);
				}
			}
			
		}
		
		for (int i = 0; i< 4; i++){
			if (nextX[i] < 0){
				crosshairs[i].setPosition(0, crosshairs[i].getY());
			} else  if (nextX[i] > EntityFactory.WorldWidth){
				crosshairs[i].setPosition(EntityFactory.WorldWidth, crosshairs[i].getY());
			} else  if (nextY[i] < 0){
				crosshairs[i].setPosition(crosshairs[i].getX(), 0);
			} else if (nextY[i] > EntityFactory.WorldHeight){
				crosshairs[i].setPosition(crosshairs[i].getX(), EntityFactory.WorldWidth);
			} else {
				crosshairs[i].setPosition(nextX[i], nextY[i]);
			}
		}
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void packageInanimateEntities() {
		inanimates = new Entity[targets.size() + explosions.size()];
		for (int i = 0; i < targets.size(); i++){
			inanimates[i] = targets.elementAt(i);
		}
		for (int i = 0; i < explosions.size(); i++){
			inanimates[i+targets.size()] = explosions.elementAt(i);
		}
	}
	
	/**
	 * Adds a target to the game at a random location.
	 */
	public void addTarget(){
		targets.addElement(entityFactory.addTarget(randomNum(10,90), randomNum(10,90), 10, 10));
	}
	
	/**
	 * Returns a random integer within a given range.
	 * @param min Minimum value of range
	 * @param max Maximum value of range
	 * @return a random integer within a given range
	 */
	private int randomNum(int min, int max){
		Random rand = new Random();
		return rand.nextInt((max-min)-1) + min;
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void packageStaticEntities() {
		statics = new Entity[]{};
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public void sortForRanking(PlayerModel[] results){
		for (int i = 0; i < scores.length; i++){
            results[i] = new PlayerModel(scores[i], 0, server.getPlayerName(i+1), i+1);
        }
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
    public void publishToDisplay(PlayerModel[] results){
		for (int i = 0; i < scores.length; i++){
            results[i].setScore(scores[i]);
            if (results[i].getPosition() == 1){
                server.updateLobby(new LobbyInput(100, results[i].getPlayerNumber()));
            }
        }
    }
	
}
