/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.game;

public class GameOverScore extends GameOverChecker {
    
    private int scoreLimit;
    private int score;
    private int[] scores;
    
    public GameOverScore(GameModel model, int scoreLimit){
        super(model);
        this.scoreLimit = scoreLimit;
        this.scores = new int[]{1,1,1,1};
    }

    public void addScore(int playerNum){
        score++;
        this.scores[playerNum-1]--;
        if (score >= scoreLimit){
        	for (int i = 0; i < 4; i++){
        		if (scores[i] == 1){
        			break;
        		}
        	}
            gameOver();
        }
    }

}
