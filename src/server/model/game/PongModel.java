/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.game;

import java.util.Iterator;
import java.util.Vector;

import io.modelInput.GameInput;
import io.modelInput.LobbyInput;
import io.modelInput.ModelTimerInput;
import server.Server;
import server.model.PlayerModel;
import server.model.entity.Entity;
import server.model.entity.EntityFactory;
import server.model.entity.ImmobileRectangle;
import server.model.entity.MobileCircle;
import server.model.entity.MobileRectangle;
import server.model.entity.PongFactory;

public class PongModel extends GameModel {

    static public double PaddleSpeed = 0.06;
    static public int MaxBalls = 20;

    private MobileRectangle[] paddles;
    private ImmobileRectangle[] corners;
    private Vector<MobileCircle> balls;
    private PongFactory entityFactory;
    
    public PongModel(Server server){
        super(server);
        
        entityFactory = new PongFactory();
        paddles = entityFactory.createPlayers();
        corners = entityFactory.createCorners();
        balls = new Vector<MobileCircle>();
        entityFactory.addBall(balls);

        players = paddles;
        inanimates = new Entity[]{};
        statics = new Entity[]{};
        
        gameOverChecker = new GameOverTimer(this, 30);

        clock.start();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    synchronized public void gameUpdate(GameInput gameInput){
        MobileRectangle r = paddles[gameInput.getPlayerNumber()-1];
        if (gameInput.getPlayerNumber() == 1 || gameInput.getPlayerNumber() == 3){
            if (r.getYSpeed() == 0){
                if (gameInput.up()){
                    r.setYSpeed(-PaddleSpeed);
                } else if (gameInput.down()){
                    r.setYSpeed(PaddleSpeed);
                } else{
                    r.setYSpeed(0);
                }
            } else if (r.getYSpeed() == PaddleSpeed){
                if (!gameInput.down()){
                    r.setYSpeed(0);
                } else if (gameInput.up()){
                    r.setYSpeed(-PaddleSpeed);
                } else{
                    r.setYSpeed(PaddleSpeed);
                }
            } else if (r.getYSpeed() == -PaddleSpeed){
                if (!gameInput.up()){
                    r.setYSpeed(0);
                } else if (gameInput.down()){
                    r.setYSpeed(PaddleSpeed);
                }else{
                    r.setYSpeed(-PaddleSpeed);
                }
            }
        } else {
            if (r.getXSpeed() == 0){
                if (gameInput.left()){
                    r.setXSpeed(-PaddleSpeed);
                } else if (gameInput.right()){
                    r.setXSpeed(PaddleSpeed);
                } else{
                    r.setXSpeed(0);
                }
            } else if (r.getXSpeed() == PaddleSpeed){
                if (!gameInput.right()){
                    r.setXSpeed(0);
                } else if (gameInput.left()){
                    r.setXSpeed(-PaddleSpeed);
                } else{
                    r.setXSpeed(PaddleSpeed);
                }
            } else if (r.getXSpeed() == -PaddleSpeed){
                if (!gameInput.left()){
                    r.setXSpeed(0);
                } else if (gameInput.right()){
                    r.setXSpeed(PaddleSpeed);
                }else{
                    r.setXSpeed(-PaddleSpeed);
                }
            }
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    synchronized public void modelTimerUpdate(ModelTimerInput tick){
        for (MobileRectangle r : paddles){
            double newX = r.getX()+(tick.getMillis() * r.getXSpeed());
            double newY = r.getY()+(tick.getMillis() * r.getYSpeed());
            r.setPosition(newX, newY);
            for (ImmobileRectangle corner : corners){
                if (corner.collide(r)){
                    corner.moveOut(r);
                }
            }
        }
        Iterator<MobileCircle> ballIterator = balls.iterator();
        int newBalls = 0;
        while (ballIterator.hasNext()){
            MobileCircle b = ballIterator.next();
            double newX = b.getX()+(tick.getMillis() * b.getXSpeed());
            double newY = b.getY()+(tick.getMillis() * b.getYSpeed());
            b.setPosition(newX, newY);
            for (ImmobileRectangle corner : corners){
                if (corner.collide(b)){
                    corner.deflect(b);
                }
            }
            for (MobileRectangle paddle : paddles){
                if (paddle.collide(b)){
                    paddle.deflect(b);
                }
            }
            if (b.getX() > EntityFactory.WorldWidth || b.getX()+b.getWidth() < 0 ||
                    b.getY() > EntityFactory.WorldHeight || b.getY()+b.getHeight() < 0){
                if (b.getX()+b.getWidth() < 0){
                    scores[0]++;
                } else if (b.getY()+b.getHeight() < 0){
                    scores[1]++;
                } else if (b.getX() > EntityFactory.WorldWidth){
                    scores[2]++;
                } else if (b.getY() > EntityFactory.WorldHeight){
                    scores[3]++;
                }
                newBalls += 2;
                ballIterator.remove();
            }
        }
        for (int i = 0; i < newBalls; i++){
            if (balls.size() <= MaxBalls){
                entityFactory.addBall(balls);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void packageInanimateEntities(){
        inanimates = new Entity[corners.length + balls.size()];
        for (int i = 0; i < corners.length; i++){
            inanimates[i] = corners[i];
        }
        for (int i = corners.length; i < corners.length+balls.size(); i++){
            inanimates[i] = balls.elementAt(i-corners.length);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void packageStaticEntities(){
        // pass
    }

    /**
     * {@inheritDoc}
     */
	@Override
	public void sortForRanking(PlayerModel[] results) {
		for (int i = 0; i < scores.length; i++){
            scores[i] = -scores[i];
            results[i] = new PlayerModel(scores[i], 0, server.getPlayerName(i+1), i+1);
        }
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public void publishToDisplay(PlayerModel[] results) {
		for (int i = 0; i < scores.length; i++){
            scores[i] = -scores[i];
            results[i].setScore(scores[i]);
            if (results[i].getPosition() == 1){
                server.updateLobby(new LobbyInput(100, results[i].getPlayerNumber()));
            }
        }
	}

}
