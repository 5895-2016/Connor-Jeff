/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.game;

import io.modelInput.GameInput;
import io.modelInput.LobbyInput;
import io.modelInput.ModelTimerInput;
import server.Server;
import server.model.PlayerModel;
import server.model.entity.Entity;
import server.model.entity.EntityFactory;
import server.model.entity.ImmobileRectangle;
import server.model.entity.MobileRectangle;
import server.model.entity.RaceFactory;

public class RaceModel extends GameModel {
    
    private RaceFactory entityFactory;
    private MobileRectangle[] rectangles;
    private ImmobileRectangle[] walls;
    private ImmobileRectangle[] finishLine;
    private MobileRectangle[] markers;
    private GameOverFlags stateChecker;
    
    public RaceModel(Server server){
        super(server);
        
        entityFactory = new RaceFactory();
        
        rectangles = entityFactory.createPlayers();
        players = rectangles;
        
        walls = entityFactory.createWalls();
        
        finishLine = entityFactory.createFinishLine();
        
        markers = entityFactory.createMarkers();
        
        stateChecker = new GameOverFlags(this); 
        gameOverChecker = stateChecker;
        
        clock.start();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void gameUpdate(GameInput gameInput) {
        if (gameInput.up() && !gameInput.down()){
            rectangles[gameInput.getPlayerNumber()-1].setYSpeed(-0.05);
        } else if (gameInput.down() && !gameInput.up()){
            rectangles[gameInput.getPlayerNumber()-1].setYSpeed(0.05);
        } else{
            rectangles[gameInput.getPlayerNumber()-1].setYSpeed(0);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void modelTimerUpdate(ModelTimerInput tick) {
        for (int i = 0; i < rectangles.length; i++){
            if (!stateChecker.getStatus(i)){
                double newX = rectangles[i].getX()+(tick.getMillis() * rectangles[i].getXSpeed());
                double newY = rectangles[i].getY()+(tick.getMillis() * rectangles[i].getYSpeed());
                if (newY > EntityFactory.WorldHeight-rectangles[i].getHeight()){
                    newY = EntityFactory.WorldHeight-rectangles[i].getHeight();
                } else if (newY < markers[0].getHeight()){
                    newY = markers[0].getHeight();
                }
                rectangles[i].setPosition(newX, newY);
                for (ImmobileRectangle wall : walls){
                    if (wall.collide(rectangles[i])){
                        wall.moveOut(rectangles[i]);
                    }
                }
                scores[i] += tick.getMillis();
                if (rectangles[i].getX() >= RaceFactory.EndDistance){
                    stateChecker.setStatus(i, true);
                }
            }
        }
        for (int i = 0; i < markers.length-1; i++){
            double newX = (rectangles[i].getX()/(RaceFactory.EndDistance-markers[i+1].getWidth())) * EntityFactory.WorldWidth;
            markers[i+1].setPosition(newX, 0);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void packageInanimateEntities() {
        inanimates = new Entity[walls.length + finishLine.length];
        for (int i = 0; i < walls.length; i++){
            inanimates[i] = walls[i];
        }
        for (int i = walls.length; i < walls.length + finishLine.length; i++){
            inanimates[i] = finishLine[i - walls.length];
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void packageStaticEntities(){
        statics = new Entity[markers.length];
        for (int i = 0; i < markers.length; i++){
            statics[i] = markers[i];
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
	public void sortForRanking(PlayerModel[] results) {
		for (int i = 0; i < scores.length; i++){
            scores[i] = -scores[i];
            results[i] = new PlayerModel(scores[i], 0, server.getPlayerName(i+1), i+1);
        }
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public void publishToDisplay(PlayerModel[] results) {
		for (int i = 0; i < scores.length; i++){
            scores[i] = -scores[i];
            results[i].setScore(scores[i]);
            if (results[i].getPosition() == 1){
                server.updateLobby(new LobbyInput(100, results[i].getPlayerNumber()));
            }
        }
	}

}
