/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.game;

public abstract class GameOverChecker {
    
    private GameModel model;
    
    protected GameOverChecker(GameModel model){
        this.model = model;
    }
    
    /**
     * Calls on {@code model} to end the current game.
     */
    protected void gameOver(){
        model.endGame();
    }

}
