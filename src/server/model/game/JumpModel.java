/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.game;

import java.util.Timer;
import java.util.TimerTask;

import io.modelInput.GameInput;
import io.modelInput.LobbyInput;
import io.modelInput.ModelTimerInput;
import server.Server;
import server.model.PlayerModel;
import server.model.entity.Entity;
import server.model.entity.EntityFactory;
import server.model.entity.ImmobileRectangle;
import server.model.entity.JumpFactory;
import server.model.entity.MobileRectangle;

public class JumpModel extends GameModel {
    
    public static double FallSpeed = 0.08;
    
    private JumpFactory entityFactory;
    private MobileRectangle[] jumpers;
    private ImmobileRectangle[] immobilePlatforms;
    private MobileRectangle[] mobilePlatforms;
    private ImmobileRectangle endToken;
    private Timer[] jumpTimers;
    private JumperState[] jumperStates;
    private GameOverFlags stateChecker;
    
    public JumpModel(Server server){
        super(server);
        
        entityFactory = new JumpFactory();
        
        jumpers = entityFactory.createPlayers();
        players = jumpers;
        
        immobilePlatforms = entityFactory.createImmobilePlatforms();
        
        mobilePlatforms = entityFactory.createMobilePlatforms();
        
        endToken = entityFactory.createEndToken();
        
        jumperStates = new JumperState[]{JumperState.SIT, JumperState.SIT, JumperState.SIT, JumperState.SIT};
        jumpTimers = new Timer[4];
        
        stateChecker = new GameOverFlags(this);
        gameOverChecker = stateChecker;
        clock.start();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void gameUpdate(GameInput gameInput) {
        if (gameInput.left() && !gameInput.right()){
            jumpers[gameInput.getPlayerNumber()-1].setXSpeed(-0.05);
        } else if (gameInput.right() && !gameInput.left()){
            jumpers[gameInput.getPlayerNumber()-1].setXSpeed(0.05);
        } else{
            jumpers[gameInput.getPlayerNumber()-1].setXSpeed(0);
        }
        if (gameInput.space()){
            if (jumperStates[gameInput.getPlayerNumber()-1] == JumperState.SIT){
                jumperStates[gameInput.getPlayerNumber()-1] = JumperState.JUMP;
                startJump(gameInput.getPlayerNumber()-1);
            }
        } else{
            if (jumperStates[gameInput.getPlayerNumber()-1] == JumperState.JUMP){
                cancelJump(gameInput.getPlayerNumber()-1);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void modelTimerUpdate(ModelTimerInput tick) {
        for (int i = 0; i < mobilePlatforms.length; i++){
            double newX = mobilePlatforms[i].getX()+(tick.getMillis() * mobilePlatforms[i].getXSpeed());
            double newY = mobilePlatforms[i].getY()+(tick.getMillis() * mobilePlatforms[i].getYSpeed());
            if (newX > EntityFactory.WorldWidth-mobilePlatforms[i].getWidth() || newX < 0){
                mobilePlatforms[i].setXSpeed(-mobilePlatforms[i].getXSpeed());
            }
            mobilePlatforms[i].setPosition(newX, newY);
        }
        for (int i = 0; i < jumpers.length; i++){
            double newX = jumpers[i].getX()+(tick.getMillis() * jumpers[i].getXSpeed());
            double newY = jumpers[i].getY()+(tick.getMillis() * jumpers[i].getYSpeed());
            if (jumpers[i].getYSpeed() < 0){
                jumpers[i].setYSpeed(0.995*jumpers[i].getYSpeed());
            } else if (jumpers[i].getYSpeed() > 0 && jumperStates[i] == JumperState.FALL){
                jumpers[i].setYSpeed(1.005*jumpers[i].getYSpeed());
            }
            if (newX > EntityFactory.WorldWidth){
                newX = -jumpers[i].getWidth();
            } else if (newX < -jumpers[i].getWidth()){
                newX = EntityFactory.WorldWidth;
            }
            jumpers[i].setPosition(newX, newY);
            if (!stateChecker.getStatus(i)){
                for (ImmobileRectangle p : immobilePlatforms){
                    if (p.collide(jumpers[i])){
                        p.moveOut(jumpers[i]);
                        if (jumpers[i].getY() < p.getY()){
                            jumperStates[i] = JumperState.SIT;
                        }
                    }
                }
                for (MobileRectangle p : mobilePlatforms){
                    if (p.collide(jumpers[i])){
                        p.moveOut(jumpers[i]);
                        if (jumpers[i].getY() < p.getY()){
                            jumperStates[i] = JumperState.SIT;
                        }
                    }
                }
                scores[i] += tick.getMillis();
                if (endToken.collide(jumpers[i])){
                    stateChecker.setStatus(i, true);
                }
            } else{
                
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void packageInanimateEntities() {
       inanimates = new Entity[immobilePlatforms.length + mobilePlatforms.length + 1];
       for (int i = 0; i < mobilePlatforms.length; i++){
           inanimates[i] = mobilePlatforms[i];
       }
       for (int i = mobilePlatforms.length; i < immobilePlatforms.length + mobilePlatforms.length; i++){
           inanimates[i] = immobilePlatforms[i - mobilePlatforms.length];
       }
       inanimates[inanimates.length-1] = endToken;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void packageStaticEntities() {
        statics = new Entity[]{};
    }
    
    /**
     * Puts a player in the JUMP state.
     * @param index the player number (0 indexed)
     */
    private void startJump(int index){
        jumperStates[index] = JumperState.JUMP;
        jumpers[index].setYSpeed(-FallSpeed);
        jumpTimers[index] = new Timer();
        jumpTimers[index].schedule(new JumpCancel(index), 800);
    }
    
    /**
     * Takes a player out of the JUMP state
     * @param index the player number (0 indexed)
     */
    private void endJump(int index){
        jumperStates[index] = JumperState.FALL;
        jumpers[index].setYSpeed(FallSpeed);
    }

    /**
     * cancels a jump before the JumpCancel timer has run out.
     * @param index the player number (0 indexed)
     */
    private void cancelJump(int index){
        jumpTimers[index].cancel();
        endJump(index);
    }

    // Timer for a player jumping. If the timer runs out, the player's jump ends.
    private class JumpCancel extends TimerTask{
        private int index;
        public JumpCancel(int index){
            this.index = index;
        }
        @Override
        public void run(){
            endJump(index);
        }
    }
    
    // States for each player which affect their behaviour.
    private enum JumperState{
        JUMP, FALL, SIT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
	public void sortForRanking(PlayerModel[] results) {
		for (int i = 0; i < scores.length; i++){
            scores[i] = -scores[i];
            results[i] = new PlayerModel(scores[i], 0, server.getPlayerName(i+1), i+1);
        }
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public void publishToDisplay(PlayerModel[] results) {
		for (int i = 0; i < scores.length; i++){
            scores[i] = -scores[i];
            results[i].setScore(scores[i]);
            if (results[i].getPosition() == 1){
                server.updateLobby(new LobbyInput(100, results[i].getPlayerNumber()));
            }
        }
	}

}
