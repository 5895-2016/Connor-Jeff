/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.game;

import java.util.Arrays;

public class GameOverFlags extends GameOverChecker {
    
    private boolean[] finished;
    
    public GameOverFlags(GameModel model){
        super(model);
        finished = new boolean[] {false, false, false, false};
    }
    
    /**
     * Sets the {@code index}'th flag to {@code status}.</br>
     * If all four flags are true, ends the game.
     * @param index
     * @param status
     */
    public void setStatus(int index, boolean status){
        finished[index] = status;
        if (Arrays.equals(finished, new boolean[]{true, true, true, true})){
            gameOver();
        }
    }
    
    /**
     * Gets the status of the {@code index}'th flag.
     * @param index
     * @return the current value of the flag.
     */
    public boolean getStatus(int index){
        return finished[index];
    }

}
