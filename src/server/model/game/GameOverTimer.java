/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.game;

import java.util.Timer;
import java.util.TimerTask;

public class GameOverTimer extends GameOverChecker {
    
    private Timer timer;
    
    public GameOverTimer(GameModel model, long seconds){
        super(model);
        timer = new Timer();
        timer.schedule(new FlagRaise(), seconds*1000);
    }
    
    // Ends the game when the timer runs out
    private class FlagRaise extends TimerTask{
        @Override
        public void run(){
            gameOver();
        }
    }

}
