/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model.game;

import io.modelInput.GameInput;
import io.modelInput.ModelTimerInput;
import io.modelInput.ViewTimerInput;
import io.viewInput.GameSnapshot;
import server.Server;
import server.model.Model;
import server.model.ModelClock;
import server.model.PlayerModel;
import server.model.entity.Entity;

public abstract class GameModel extends Model {
    
    protected Server server;
    
    protected Entity[] players;
    protected Entity[] inanimates;
    protected Entity[] statics;
    protected ModelClock clock;
    protected int[] scores;
    
	protected GameOverChecker gameOverChecker;

    /**
     * Constructor for GameModel
     * @param server
     */
    protected GameModel(Server server){
        super(server);
        this.server = server;
        
        scores = new int[] {0, 0, 0, 0};

        clock = new ModelClock(this);
    }
    
    /**
     * Applies user input to the players' entities.
     * @param gameInput - The user's input state
     */
    public abstract void gameUpdate(GameInput gameInput);
    
    /**
     * Updates the model entities on clock tick
     * @param tick - Model tick
     */
    public abstract void modelTimerUpdate(ModelTimerInput tick);
    
    /**
     * Takes a snapshot of the model entities and sends it to the clients.
     * @param tick - View tick
     */
    public void viewTimerUpdate(ViewTimerInput tick){
        packageInanimateEntities();
        packageStaticEntities();
        GameSnapshot snapshot = new GameSnapshot(players, inanimates, statics);
        updateClients(snapshot);
    }
    
    /**
     * Fills the {@code inanimates} array with all of the inanimate entities to be drawn on the screen.
     */
    public abstract void packageInanimateEntities();
    
    /**
     * Fills the {@code statics} array with all of the entities to be drawn statically on the screen.
     */
    public abstract void packageStaticEntities();
    
    /**
     * Changes state to results.
     */
    synchronized public void endGame(){
        clock.dispose();
        server.startResults(calculateResults());
    }
    
    /**
     * Calculates the results of the game.
     * @return An array of PlayerModels that contain their current mini-game score and position.
     */
    private PlayerModel[] calculateResults(){
        PlayerModel[] results = new PlayerModel[4];
        sortForRanking(results);
        calcPositions(scores, results);
        results = sortPlayers(results);
        publishToDisplay(results);
        return results;
    }
    
    /**
     * modify the {@code score} array so that they ascend in order of rank in the game. 
     * @param results An array of {@code PlayerModel}s with the modified scores.
     */
    public abstract void sortForRanking(PlayerModel[] results);
    
    /**
     * return the scores in {@code scores} to their correct values, set the scores in {@code results} to the correct values, and add 100 to the winner's metascore. 
     * @param results An array of {@code PlayerModel}s with the actual scores.
     */
    public abstract void publishToDisplay(PlayerModel[] results);
}
