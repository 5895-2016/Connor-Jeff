/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model;

import game.GameState;
import server.Server;

public class ModelFactory {
    
    Server server;
    GameModelFactory gameFactory;
    
    /**
     * Constructor for ModelFactory
     * @param server - The server that creates the ModelFactory.
     */
    public ModelFactory(Server server){
        this.server = server;
        gameFactory = new GameModelFactory(server);
    }
    
    /**
     * Creates a model based on what the current state is.
     * @param state - The current state
     * @return The model thats been created.
     */
    public Model create(GameState state){
        if (state == GameState.GAMELOBBY){
            return new LobbyModel(server);
        }
        else if (state == GameState.PREVIEW){
            return gameFactory.createPreview();
        }
        else if (state == GameState.MINIGAME){
            return gameFactory.createGame();
        }
        else if (state == GameState.RESULTS){
            return new ResultsModel(server);
        }
        else if (state == GameState.WINSCREEN){
            return new WinscreenModel(server);
        }
        else{
            System.out.println("ModelFactory received unknown state!");
            return null;
        }
    }
    
    /**
     * Tells the game factory to choose the next game to be played.
     */
    public void chooseNewMinigame(){
        gameFactory.chooseNext();
    }
    
    /**
     * Returns a boolean indicating if the current game has finished.
     * @return a boolean indicating if the current game has finished.
     */
    public boolean finished(){
        return gameFactory.finished();
    }

}
