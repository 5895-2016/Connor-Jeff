/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import game.MiniGame;
import server.Server;
import server.model.game.CODMW4Model;
import server.model.game.GameModel;
import server.model.game.JumpModel;
import server.model.game.PongModel;
import server.model.game.RaceModel;
import server.model.game.TargetShooterModel;
import server.model.preview.CODMW4PreviewModel;
import server.model.preview.JumpPreviewModel;
import server.model.preview.PongPreviewModel;
import server.model.preview.PreviewModel;
import server.model.preview.RacePreviewModel;
import server.model.preview.TargetShooterPreviewModel;

public class GameModelFactory {
    
    Server server;
    MiniGame[] games;
    int currentGameIndex;
    MiniGame currentGame;
    private boolean finished;
    
    public GameModelFactory(Server server){
        this.server = server;
        // Randomly sort every minigame
        List<MiniGame> g = Arrays.asList(MiniGame.values());
        Collections.shuffle(g);
        games = (MiniGame[]) g.toArray();
        //games = new MiniGame[] {MiniGame.TARGETSHOOTER}; // Test a game
        currentGameIndex = -1;
        finished = false;
    }
    
    public void chooseNext(){
        currentGame = games[++currentGameIndex];
        if (currentGameIndex == games.length-1){
            finished = true;
            currentGameIndex = -1;
        }
        server.setCurrentMinigame(currentGame);
    }
    
    public boolean finished(){
        return finished;
    }
    
    public PreviewModel createPreview(){
        if (currentGame == MiniGame.PONG){
            return new PongPreviewModel(server);
        } 
        if (currentGame == MiniGame.TARGETSHOOTER){
            return new TargetShooterPreviewModel(server);
        }else if(currentGame == MiniGame.COD){
            return new CODMW4PreviewModel(server);
        } else if(currentGame == MiniGame.RACE){
            return new RacePreviewModel(server);
        } else if(currentGame == MiniGame.JUMP){
            return new JumpPreviewModel(server);
        } else{
            System.out.println("GameModelFactory received unknown game!");
            return null;
        }
    }
    
    public GameModel createGame(){
        if (currentGame == MiniGame.PONG){
            return new PongModel(server);
        } else if (currentGame == MiniGame.TARGETSHOOTER){
        	return new TargetShooterModel(server);
        } else if (currentGame == MiniGame.COD){
        	return new CODMW4Model(server);
        } else if(currentGame == MiniGame.RACE){
            return new RaceModel(server);
        } else if(currentGame == MiniGame.JUMP){
            return new JumpModel(server);
        } else{
            System.out.println("GameModelFactory received unknown game!");
            return null;
        }
    }

}
