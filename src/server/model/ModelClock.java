/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model;

import java.util.Timer;
import java.util.TimerTask;

import io.modelInput.ModelTimerInput;
import io.modelInput.ViewTimerInput;

public class ModelClock {

    static public long ModelTimerPeriod = 5;
    static public long ViewTimerPeriod = 30;
    
    private Model model;
    private Timer modelUpdateTimer;
    private Timer viewUpdateTimer;
    
    /**
     * Constructor for ModelClock.
     * @param m - the model that creates the ModelClock.
     */
    public ModelClock(Model m){
        this.model = m;
        modelUpdateTimer = new Timer();
        viewUpdateTimer = new Timer();
    }
    
    /**
     * Begins the model and view timers.
     */
    public void start(){
        modelUpdateTimer.scheduleAtFixedRate(new ModelTick(ModelTimerPeriod), 0, ModelTimerPeriod);
        viewUpdateTimer.scheduleAtFixedRate(new ViewTick(ViewTimerPeriod), 0, ViewTimerPeriod);
    }
    
    /**
     * Cancels the model and view timers.
     */
    public void dispose(){
        modelUpdateTimer.cancel();
        viewUpdateTimer.cancel();
    }

    private class ModelTick extends TimerTask{
        
        private long period;
        
        public ModelTick(long period){
            super();
            this.period = period;
        }
        
        @Override
        public void run(){
            model.updateModel(new ModelTimerInput(period));
        }
    }

    private class ViewTick extends TimerTask{
        
        private long period;
        
        public ViewTick(long period){
            super();
            this.period = period;
        }
        
        @Override
        public void run(){
            model.updateModel(new ViewTimerInput(period));
        }
    }

}
