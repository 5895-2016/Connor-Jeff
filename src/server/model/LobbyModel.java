/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model;

import java.util.*;

import io.modelInput.ChatInput;
import io.modelInput.LobbyInput;
import io.modelInput.ReadyInput;
import io.modelInput.ScoreRequest;
import io.viewInput.ChatOutput;
import io.viewInput.LobbyOutput;
import io.viewInput.PlayerAddition;
import io.viewInput.ReadyOutput;
import io.viewInput.ViewInput;
import server.Server;

public class LobbyModel extends Model {
	
	private Vector <String> messages;
	private Server server;
	private int[] playerScores = {0,0,0,0};
	private PlayerModel[] players;
	private PlayerModel sortedPlayers[] = {null,null,null,null};
	private boolean[] playerReady;
	private Timer exitTimer;
	private int visiblePlayerCount;
	
	/**
	 * Constructor for LobbyModel.
	 * @param server - The server that creates this model.
	 * @throws  
	 */
	public LobbyModel(Server server){
	    super(server);
		messages = new Vector <String>();
		this.server = server;
        playerReady = new boolean[] {false, false, false, false};
        visiblePlayerCount = 1;
		
		PlayerModel player1 = new PlayerModel(0,1, server.getPlayerName(1), 1);
		PlayerModel player2 = new PlayerModel(0,1, "player2", 2);
		PlayerModel player3 = new PlayerModel(0,1, "player3", 3);
		PlayerModel player4 = new PlayerModel(0,1, "player4", 4);
		players = new PlayerModel[4];
		players[0] = player1;
		players[1] = player2;
		players[2] = player3;
		players[3] = player4;
		sortedPlayers = players;
		
		exitTimer = new Timer();
	}
	
	public PlayerModel[] getPlayerRankings(){ return players; }
		
	/**
	 * Updates the model with a new chat message and then updates the clients.
	 * @param chatInput - A string message created by a user.
	 */
	public void chatUpdate(ChatInput chatInput){
        String message = chatInput.getChatMessage();
        messages.add(message);
        ChatOutput chatOutput = new ChatOutput(message);
        
        updateClients(chatOutput);
	}
	
	/**
	 * Adds a new score to a specified players current score and updates the current position of each player.
	 * @param lobbyInput - Contains a player number and a score to be added to the players current score.
	 */
	public void lobbyUpdate(LobbyInput lobbyInput){
		int score = lobbyInput.playerScore;
		int player = lobbyInput.playerNumber;
		players[player - 1].setScore(players[player - 1].getScore() + score);
		updateScoresPositions();
	}
	
	/**
	 * Updates the clients with the current players
	 * @param s
	 */
	public void scoreUpdate(ScoreRequest s){
        LobbyOutput lobbyOutput = new LobbyOutput(sortedPlayers);
        updateClients(lobbyOutput);
	}
	
	public void addPlayer(){
		players[visiblePlayerCount].setName(server.getPlayerName(visiblePlayerCount+1));
		sortedPlayers[visiblePlayerCount].setName(server.getPlayerName(visiblePlayerCount+1));
	    visiblePlayerCount++;
	    updateClients(new LobbyOutput(sortedPlayers));
	    updateClients(new PlayerAddition(visiblePlayerCount));
	}
	
	/**
	 * Updates the players and sortedPlayers
	 */
	synchronized private void updateScoresPositions(){	
		for (int i = 0; i < playerScores.length; i++){
			playerScores[i] = players[i].getScore();
		}
		calcPositions(playerScores, players);
		sortedPlayers = sortPlayers(players);
	}
	
	
	public void setTimer(){
        exitTimer.schedule(new ExitTask(), 1000);
	}

	private class ExitTask extends TimerTask{
	    @Override
	    public void run(){
            for (int i = 0; i < playerReady.length; i++){
                playerReady[i] = false;
            }
	        server.startPreview();
	    }
	}
	
	/**
	 * Notifies model that a specified player is ready and then notifies the clients.
	 * @param input - Contains a number that specifies what player is ready.
	 */
	public void updateReadyCheck(ReadyInput input){
		int playerNumber = input.getPlayerNumber();
        playerReady[playerNumber-1] = true;
        ViewInput output = new ReadyOutput(playerNumber);
        updateClients(output);
        if (Arrays.equals(playerReady, new boolean[]{true, true, true, true})){
            setTimer();
        }
	}
	
	public PlayerModel[] getSortedPlayers(){
	    return sortedPlayers;
	}
	
}
