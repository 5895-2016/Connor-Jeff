/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */


package server.model;

import java.util.Arrays;

import io.modelInput.ModelInput;
import io.viewInput.ViewInput;
import server.Server;

public abstract class Model {
    
    Server server;
    
    /**
     * Constructor for Model.
     * @param server - The server that creates the Model.
     */
    protected Model(Server server){
        this.server = server;
    }
	
    /**
     * Updates the clients with a view input created by the model.
     * @param viewInput - An input to a view.
     */
	public void updateClients(ViewInput viewInput){
        server.updateClients(viewInput);
	}

	/**
	 * Updates the model by making the model input apply itself.
	 * @param modelInput - An input to a model.
	 */
    synchronized public void updateModel(ModelInput modelInput){
        modelInput.applyInput(this);
    }
    
    /**
     * Calculates the position of each player based on what their current score is.
     * @param playerScores - Current scores of each player
     * @param players - Contains each PlayerModel
     */
    protected void calcPositions(int[] playerScores, PlayerModel[] players){
        Arrays.sort(playerScores);
        for(int i = 0; i < playerScores.length/2; i++){
            int replace = playerScores[i];
            playerScores[i] = playerScores[playerScores.length - i - 1];
            playerScores[playerScores.length - i - 1] = replace;
        }
        int place = 1;
        for(int i = 0; i < playerScores.length; i++){
            int count = 0;
            for (PlayerModel player : players){
                if (player.getScore() == playerScores[i]){
                    player.setPosition(place);
                    count++;
                }
            }
            i += count - 1;
            place += count;
        }
    }
    
    /**
     * Sorts the players into descending order based on their current position.
     * @param players
     * @return An array of players sorted into descending order based on their current position.
     */
    protected PlayerModel[] sortPlayers(PlayerModel[] players){
        PlayerModel[] sortedPlayers = new PlayerModel[4];
        for (int i = 0; i < 4; i++){
            sortedPlayers[i] = players[i];
        }
        boolean swapped = true;
        while (swapped == true){
            swapped = false;
            for (int i = 0; i < 3; i++){
                if (sortedPlayers[i].getScore() < sortedPlayers[i+1].getScore()){
                    PlayerModel temp = sortedPlayers[i];
                    sortedPlayers[i] = sortedPlayers[i+1];
                    sortedPlayers[i+1] = temp;
                    swapped = true;
                }
            }
        }
        return sortedPlayers;
    }
}
