/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package server.model;

import java.io.Serializable;

public class PlayerModel implements Serializable {
    
    private static final long serialVersionUID = 11111L; 
    
	private int score;
	private int position;
	private String name;
	private int playerNumber;
	
	/**
	 * Constructor for PlayerModel
	 * @param score - The players current score
	 * @param position - The players current position
	 * @param name - The players name
	 * @param playerNumber - The players number
	 */
	public PlayerModel(int score, int position, String name, int playerNumber){
		this.score = score;
		this.position = position;
		this.name = name;
		this.playerNumber = playerNumber;
	}
	
	public void setScore(int score) { this.score = score; }
	public int getScore(){ return score; }
	
	public void setPosition(int position){ this.position = position; }
	public int getPosition(){ return position; }
	
	public void setName(String name){ this.name = name; }
	public String getName(){ return name; }
	
	public void setPlayerNumber(int number){ this.playerNumber = number; }
	public int getPlayerNumber(){ return playerNumber; }
}
