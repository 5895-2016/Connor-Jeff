/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package game;

// To be incorperated. Will be used to identify the different minigames.
public enum MiniGame {
    PONG, RACE, JUMP,TARGETSHOOTER, COD;
	
}
