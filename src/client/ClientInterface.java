/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client;

import java.rmi.Remote;
import java.rmi.RemoteException;

import game.GameState;
import game.MiniGame;
import io.viewInput.ViewInput;

public interface ClientInterface extends Remote {

    public void changeState(GameState newState) throws RemoteException;
    
    public void setPlayerNumber(int number) throws RemoteException;
    
    public String getPlayerName() throws RemoteException;
    
    public void updateView(ViewInput viewInput) throws RemoteException;
    
    public void setCurrentMinigame(MiniGame game) throws RemoteException;
    
}
