/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view;

import java.awt.Color;

import javax.swing.JFrame;

import io.viewInput.ViewInput;

@SuppressWarnings("serial")
public abstract class View extends JFrame {
    
    // Colours for each player
    static public Color[] playerColors = new Color[] {new Color(37, 116, 169), new Color(248, 148, 6),
                                                      new Color(231, 76, 60), new Color(38, 166, 91)};
    
    protected View(int width, int height){
        setSize(width, height);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
    }
    
    /**
     * Updates the view by making the view input apply itself.
     * @param viewInput An input to a view
     */
    synchronized public void updateView(ViewInput viewInput){
        viewInput.applyInput(this);
    }

}