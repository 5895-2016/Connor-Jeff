/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view;

import client.Client;
import game.GameState;
import game.MiniGame;

public class ViewFactory {
    
    Client owner;
    GameViewFactory gameFactory;
    
    public ViewFactory(Client client){
        owner = client;
        gameFactory = new GameViewFactory(owner);
    }
    
    /**
     * Creates a {@code View} corresponding to {@code GameState} that is passed in.
     * @param state the view subclass to create.
     * @return a View whose type corresponds to <b>state</b>.
     */
    public View create(GameState state){
        if (state == GameState.MAINMENU){
            return new MainMenuView(owner);
        }
        else if (state == GameState.GAMELOBBY){
            return new LobbyView(owner);
        }
        else if (state == GameState.PREVIEW){
            return gameFactory.createPreview();
        }
        else if (state == GameState.MINIGAME){
            return gameFactory.createGame();
        }
        else if (state == GameState.RESULTS){
            return gameFactory.createResults();
        }
        else if (state == GameState.WINSCREEN){
            return new WinscreenView(owner);
        }
        else{
            System.out.println("ViewFactory received unknown state!");
            return null;
        }
    }
    
    /**
     * Tell the {@code GameViewFactory} to set the current game to a new game.
     * @param game New game to be set
     */
    public void setCurrentGame(MiniGame game){
        gameFactory.setCurrentGame(game);
    }

}
