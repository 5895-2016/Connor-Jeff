/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view;

import client.Client;
import client.view.game.CODMW4View;
import client.view.game.GameView;
import client.view.game.JumpView;
import client.view.game.PongView;
import client.view.game.RaceView;
import client.view.game.TargetShooterView;
import client.view.preview.CODMW4PreviewView;
import client.view.preview.JumpPreviewView;
import client.view.preview.PongPreviewView;
import client.view.preview.PreviewView;
import client.view.preview.RacePreviewView;
import client.view.preview.TargetShooterPreviewView;
import game.MiniGame;

public class GameViewFactory {
    
    Client owner;
    MiniGame currentGame;
    
    public GameViewFactory(Client owner){
        this.owner = owner;
    }
    
    public void setCurrentGame(MiniGame game){
        currentGame = game;
    }
    
    public PreviewView createPreview(){
        if (currentGame == MiniGame.PONG ){
            return new PongPreviewView(owner);
        } else if (currentGame == MiniGame.TARGETSHOOTER){
        	return new TargetShooterPreviewView(owner);
        } else if(currentGame == MiniGame.COD){
            return new CODMW4PreviewView(owner);
        }else if(currentGame == MiniGame.RACE){
            return new RacePreviewView(owner);
        } else if(currentGame == MiniGame.JUMP){
            return new JumpPreviewView(owner);
        } else{
            System.out.println("GameViewFactory received unknown game!");
            return null;
        }
    }
    
    public GameView createGame(){
        if (currentGame == MiniGame.PONG){
            return new PongView(owner);
        } else if (currentGame == MiniGame.TARGETSHOOTER){
        	return new TargetShooterView(owner);
        } else if (currentGame == MiniGame.COD) {
        	return new CODMW4View(owner);
        } else if(currentGame == MiniGame.RACE){
            return new RaceView(owner);
        } else if(currentGame == MiniGame.JUMP){
            return new JumpView(owner);
        }  else{
            System.out.println("GameViewFactory received unknown game!");
            return null;
        }
    }
    
    public ResultsView createResults(){
        if (currentGame == MiniGame.PONG){
            return new ResultsView(owner, "GOALS SCORED ON");
        } else if (currentGame == MiniGame.TARGETSHOOTER){
        	return new ResultsView(owner, "TARGETS SHOT");
        } else if(currentGame == MiniGame.COD){
            return new ResultsView(owner, "Lives Remaining");
        } else if(currentGame == MiniGame.RACE){
            return new ResultsView(owner, "TIME (ms)");
        } else if(currentGame == MiniGame.JUMP){
            return new ResultsView(owner, "TIME (ms)");
        }  else{
            System.out.println("GameViewFactory received unknown game!");
            return null;
        }
    }

}
