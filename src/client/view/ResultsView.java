/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import client.Client;
import client.controller.ResultsController;
import io.viewInput.ReadyOutput;
import io.viewInput.ResultsOutput;

@SuppressWarnings("serial")
public class ResultsView extends View {
    
    static private int buttonHeight = 30;
    static private int buttonWidth = 200;
    static private int readyOffset = 50;

	private JLabel[] playerNames = new JLabel[4];
	private JLabel[] scores = new JLabel[4];
	private JLabel[] places = new JLabel[4];
	private JLabel[] readyLabels = new JLabel[4];
	private JPanel[] namePanels = new JPanel[4];
	private JPanel[] scorePanels = new JPanel[4];
	private JPanel[] placePanels = new JPanel[4];
	private JPanel[] titlePanels = new JPanel[3];
	
	public ResultsView(Client client, String scoreTitleString) {
		
		super(Client.viewWidth, Client.viewHeight);
		setLocation(((client.getPlayerNumber()-1) % 2) * Client.viewWidth,
                ((client.getPlayerNumber()-1) / 2) * Client.viewHeight);	
		
		//Ready Labels
		for (int i = 0; i < 4; i++){
			readyLabels[i] = new JLabel("READY");
			readyLabels[i].setBounds(230, 135 + (i*50),readyLabels[i].getPreferredSize().width,readyLabels[i].getPreferredSize().height);
			readyLabels[i].setVisible(false);
			add(readyLabels[i]);
		}
		
		
		// titles for each table column.
		JLabel nameTitle = new JLabel("PLAYER");
		JLabel rankTitle = new JLabel("RANK");
		JLabel scoreTitle = new JLabel(scoreTitleString);
		JLabel[] titles = {nameTitle, scoreTitle, rankTitle};
		for(int i = 0; i < 3; i++){
			titlePanels[i] = new JPanel();
			titlePanels[i].setBounds(25 + 150*i, 50, 150, 50);
			titlePanels[i].add(titles[i]);
			titlePanels[i].setBackground(new Color(236, 100, 75));
			this.add(titlePanels[i]);
		}
		
		// panels representing each cell in the table
		for(int i = 0; i < 4; i++){
			playerNames[i] = new JLabel();
			scores[i] = new JLabel();
			places[i] = new JLabel();
			namePanels[i] = new JPanel();
			scorePanels[i] = new JPanel();
			placePanels[i] = new JPanel();
			
			playerNames[i].setText("Player" + (i+1));
			scores[i].setText("0");
			places[i].setText("1");
			
			namePanels[i].setBounds(25, 110 + (50*i), 150, 50);
			namePanels[i].add(playerNames[i]);
			namePanels[i].setBackground(playerColors[i]);
			scorePanels[i].setBounds(175, 110 + (50*i), 150, 50);
			scorePanels[i].add(scores[i]);
			scorePanels[i].setBackground(playerColors[i]);
			placePanels[i].setBounds(325, 110 + (50*i), 150, 50);
			placePanels[i].add(places[i]);
			placePanels[i].setBackground(playerColors[i]);
			
			this.add(namePanels[i]);
			this.add(scorePanels[i]);
			this.add(placePanels[i]);
		}
		
		// controller
		ResultsController controller = new ResultsController(client);
		
		// ready button
        JButton readyButton = new JButton("READY");
        readyButton.setBounds(getWidth()/2 - buttonWidth/2, getHeight() - readyOffset - buttonHeight*2, 
                              buttonWidth, buttonHeight);
        readyButton.addActionListener(controller);
        add(readyButton);
		
		setVisible(true);
		controller.requestScore();
	}
	
	/**
	 * Rearranges the table to reflect the results of the previous minigame.
	 * @param resultsOutput contains a {@code PlayerModel} for each player in order of ranking for the minigame..
	 */
	public void resultsUpdate(ResultsOutput resultsOutput){
	    for (int i = 0; i < resultsOutput.getPlayers().length; i++){
            playerNames[i].setText(resultsOutput.getPlayers()[i].getName());
            scores[i].setText(Integer.toString(resultsOutput.getPlayers()[i].getScore()));
            places[i].setText(Integer.toString(resultsOutput.getPlayers()[i].getPosition()));
            namePanels[i].setBackground(playerColors[resultsOutput.getPlayers()[i].getPlayerNumber()-1]);
            scorePanels[i].setBackground(playerColors[resultsOutput.getPlayers()[i].getPlayerNumber()-1]);
            placePanels[i].setBackground(playerColors[resultsOutput.getPlayers()[i].getPlayerNumber()-1]);
            readyLabels[resultsOutput.getPlayers()[i].getPlayerNumber()-1].setBounds(230, 135 + (i*50),
                                     readyLabels[i].getPreferredSize().width, readyLabels[i].getPreferredSize().height);
	    }
	}
	
	/**
	 * Sets the given players ready label to visible.
	 * @param resultsOutput
	 */
	public void updateReadyCheck(ReadyOutput resultsOutput){
		int num =  resultsOutput.getPlayerNumber() - 1;
		readyLabels[num].setVisible(true);
	}
	
}
