/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view.preview;

import javax.swing.JLabel;

import client.Client;
import client.view.animation.AnimationPanel;
import io.viewInput.PreviewSnapshot;

@SuppressWarnings("serial")
public class RacePreviewView extends PreviewView {
    
    private Client client;
    
    public RacePreviewView(Client client) {
        super(client);
        setTitle("Sanic Gaem!");
        this.client = client;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AnimationPanel createAnimationPanel() {
        return new AnimationPanel("sanicbg.png");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JLabel createInstructions() {
        return new JLabel("Dodge the walls to race the other players to the finish!");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JLabel createControls() {
        return  new JLabel("Press the up and down keys to move up and down.");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void applyOffset(PreviewSnapshot snapshot){
        double playerXPosition = snapshot.getPlayerEntities()[client.getPlayerNumber()-1].getX();
        panel.setXOffset(panel.transformXCoordinate(playerXPosition) - (panel.getWidth()/2));
    }

}
