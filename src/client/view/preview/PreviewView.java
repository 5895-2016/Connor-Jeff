/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view.preview;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JLabel;

import client.Client;
import client.controller.PreviewButtonListener;
import client.view.View;
import client.view.animation.AnimationPanel;
import client.view.animation.Drawable;
import io.viewInput.PreviewSnapshot;
import io.viewInput.ReadyOutput;
import server.model.PlayerModel;

@SuppressWarnings("serial")
public abstract class PreviewView extends View {

    static private int buttonHeight = 50;
    static private int buttonWidth = 100;
    static private int readyOffset = 10;
    static private float fadeAlpha = 0.15f;

    private JLabel[] players;
    protected AnimationPanel panel;

    public PreviewView(Client client){
        super(Client.viewWidth, Client.viewHeight);
        setLocation(((client.getPlayerNumber()-1) % 2) * Client.viewWidth,
                    ((client.getPlayerNumber()-1) / 2) * Client.viewHeight);
        
        // ready label
        JLabel readyLabel = new JLabel("Ready:");
        readyLabel.setBounds(readyOffset, 0, readyLabel.getPreferredSize().width, 
                                    readyLabel.getPreferredSize().height);
        add(readyLabel);
        
        // Display each player's name
        players = new JLabel[4];
        for (PlayerModel p : client.getPlayers()){
            int i = p.getPlayerNumber()-1;
            players[i] = new JLabel(p.getName());
            // Make each name faded until the player is ready
            players[i].setForeground(new Color((float) View.playerColors[i].getRed()/255,
                                               (float) View.playerColors[i].getGreen()/255,
                                               (float) View.playerColors[i].getBlue()/255,
                                               fadeAlpha));
            players[i].setBounds(readyOffset, players[i].getPreferredSize().height * (i+1), 
                                     players[i].getPreferredSize().width, 
                                     players[i].getPreferredSize().height);
            add(players[i]);
        }
        
        // ready button
        JButton readyButton = new JButton("READY");
        readyButton.setBounds(readyOffset, Client.viewHeight/2 - buttonHeight*2, 
                              buttonWidth, buttonHeight);
        readyButton.addActionListener(new PreviewButtonListener(client, "READY"));
        add(readyButton);
        
        // animation panel
        panel = createAnimationPanel();
        panel.setBounds(Client.viewWidth/4, 0, (Client.viewWidth*3)/4, (Client.viewHeight*3)/4);
        add(panel);
        panel.repaint();

        // Instructions and controls for the minigame
        JLabel instructions = createInstructions();
        JLabel controls = createControls();
        instructions.setBounds(readyOffset, (Client.viewWidth*3)/4 + readyOffset,
                               instructions.getPreferredSize().width, 
                               instructions.getPreferredSize().height);
        controls.setBounds(readyOffset,
                          (Client.viewWidth*3)/4 + readyOffset + instructions.getPreferredSize().height,
                           controls.getPreferredSize().width, 
                           controls.getPreferredSize().height);
        add(instructions);
        add(controls);
        
        setVisible(true);
    }

    /**
     * Updates the view to show a player is ready.
     * @param previewOutput {@code ReadyOutput} indicating which player is now ready.
     */
    public void previewUpdate(ReadyOutput previewOutput){
        int playerNumber = (previewOutput).getPlayerNumber();
        players[playerNumber-1].setForeground(View.playerColors[playerNumber-1]);
        repaint();
    }

    /**
     * Takes each entity in the {@code PreviewSnapshot}, creates their equivalent drawables, 
     * sends them to the panel, then tells the panel to repaint itself.
     * @param snapshot A snapshot containing each entity to be drawn.
     */
    // TODO: Combine the game and preview animation methods and their respective snapshot classes.
    public void animationUpdate(PreviewSnapshot snapshot){
        Drawable[] players = new Drawable[snapshot.getPlayerEntities().length];
        Drawable[] objects = new Drawable[snapshot.getInanimateEntities().length];
        for (int i = 0; i < snapshot.getPlayerEntities().length; i++) {
            players[i] = panel.createDrawable(snapshot.getPlayerEntities()[i], View.playerColors[i]);
        }
        for (int i = 0; i < snapshot.getInanimateEntities().length; i++) {
            objects[i] = panel.createDrawable(snapshot.getInanimateEntities()[i], Color.BLACK);
        }
        applyOffset(snapshot);
        panel.setPlayers(players);
        panel.setObjects(objects);
        panel.repaint();
    }
    
    /**
     * Creates the animation panel with the correct background image.
     * @return An AnimationPanel with the correct background image.
     */
    public abstract AnimationPanel createAnimationPanel();
    
    /**
     * Creates the string for the game's instructions which are displayed.
     * @return A string containing the game's instructions.
     */
    public abstract JLabel createInstructions();

    /**
     * Creates the string describing the game's controls to be displayed.
     * @return A string containing a description of the game's controls.
     */
    public abstract JLabel createControls();
    
    /**
     * Applies an offset to the AnimationPanel to follow entities that move outside the initial frame.
     * @param snapshot a snapshot of the current entity positions.
     */
    public abstract void applyOffset(PreviewSnapshot snapshot);

}
