/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view.preview;

import javax.swing.JLabel;

import client.Client;
import client.view.animation.AnimationPanel;
import io.viewInput.PreviewSnapshot;

@SuppressWarnings("serial")
public class TargetShooterPreviewView extends PreviewView {

	public TargetShooterPreviewView(Client client) {
		super(client);
        setTitle("John Marston's Target Shooter!");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AnimationPanel createAnimationPanel() {
        return new AnimationPanel("TargetShooterBackground.png");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
    public JLabel createInstructions() {
        return new JLabel("Shoot the targets quicker than your opponents.");
    }

	/**
	 * {@inheritDoc}
	 */
    @Override
    public JLabel createControls() {
        return  new JLabel("Use the mouse to move your crosshair and click to fire. Goodluck Cowboy!");
    }

    /**
	 * {@inheritDoc}
	 */
	@Override
	public void applyOffset(PreviewSnapshot snapshot) {
		
	}
	
}
