/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view.preview;

import javax.swing.JLabel;

import client.Client;
import client.view.animation.AnimationPanel;
import io.viewInput.PreviewSnapshot;

@SuppressWarnings("serial")
public class JumpPreviewView extends PreviewView {
    
    private Client client;
    private int yOffset;
    
    public JumpPreviewView(Client client){
        super(client);
        setTitle("The One Where You Must Jump Quicker Than Everyone Else!");
        this.client = client;
        yOffset = 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AnimationPanel createAnimationPanel() {
        return new AnimationPanel("jumpbg.png");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JLabel createInstructions() {
        return new JLabel("Jump or die loser");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JLabel createControls() {
        return new JLabel("Press the space button to jump and the arrow keys to move");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void applyOffset(PreviewSnapshot snapshot) {
        double playerYPosition = snapshot.getPlayerEntities()[client.getPlayerNumber()-1].getY();
        if (panel.transformYCoordinate(playerYPosition) < yOffset + panel.getHeight()/2){
            yOffset = panel.transformYCoordinate(playerYPosition) - panel.getHeight()/2;
            panel.setYOffset(yOffset);
        } else if (panel.transformYCoordinate(playerYPosition) > yOffset + 17*panel.getHeight()/20){
            yOffset = panel.transformYCoordinate(playerYPosition) - 17*panel.getHeight()/20;
            panel.setYOffset(yOffset);
        }
    }

}
