/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view.preview;

import javax.swing.JLabel;

import client.Client;
import client.view.animation.AnimationPanel;
import io.viewInput.PreviewSnapshot;

@SuppressWarnings("serial")
public class PongPreviewView extends PreviewView {
    
    public PongPreviewView(Client client) {
        super(client);
        setTitle("Extreme Pong!");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AnimationPanel createAnimationPanel() {
        return new AnimationPanel("pongNet.png");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JLabel createInstructions() {
        return new JLabel("Keep balls out of your net by bouncing them into your opponents' nets.");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JLabel createControls() {
        return  new JLabel("Use the arrow keys to move back and forth.");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void applyOffset(PreviewSnapshot snapshot){
        // pass
    }

}
