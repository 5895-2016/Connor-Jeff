/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view.preview;

import javax.swing.JLabel;

import client.Client;
import client.view.animation.AnimationPanel;
import io.viewInput.PreviewSnapshot;

@SuppressWarnings("serial")
public class CODMW4PreviewView extends PreviewView {

	public CODMW4PreviewView(Client client) {
		super(client);
		setTitle("Call of Duty Modern Warfare 4!");
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
    public AnimationPanel createAnimationPanel() {
        return new AnimationPanel("SkullArena.png");
    }

	/**
	 * {@inheritDoc}
	 */
    @Override
    public JLabel createInstructions() {
        return new JLabel("Kill your bros before they kill you ;)");
    }

    /**
	 * {@inheritDoc}
	 */
    @Override
    public JLabel createControls() {
        return new JLabel("Move with WASD or Arrow keys, aim with mouse, and shoot by clicking");
    }
    
    /**
	 * {@inheritDoc}
	 */
	@Override
	public void applyOffset(PreviewSnapshot snapshot) {
		// TODO Auto-generated method stub

	}

}
