/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import client.Client;

@SuppressWarnings("serial")
public class MainMenuPanel extends JPanel {
    
    private BufferedImage image;
    
    public MainMenuPanel(Client client){
        try{
        File f = new File(getClass().getResource("/client/view/images/title.png").toURI());
            image = ImageIO.read(f);
        } catch (URISyntaxException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
    
    /**
     * Draws the title image.
     */
    public void paintComponent(Graphics g){
        Graphics2D g2d = (Graphics2D) g;
        AffineTransform trans = new AffineTransform(g2d.getTransform());
        trans.scale( (double) getWidth()/image.getWidth(), (double) getHeight()/image.getHeight());
        g2d.drawImage(image,
                new AffineTransformOp(trans, AffineTransformOp.TYPE_NEAREST_NEIGHBOR),
                0, 0);
    }

}
