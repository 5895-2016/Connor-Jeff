/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view.animation;

import java.awt.Color;
import java.awt.Graphics;

public class Rectangle implements Drawable {
    
    private int x;
    private int y;
    private int width;
    private int height;
    private Color color;
    
    /**
     * @param width The width in pixels.
     * @param height The height in pixels.
     * @param c The rectangle's colour.
     */
    public Rectangle(int width, int height, Color c){
        this.width = width;
        this.height = height;
        this.color = c;
    }
    
    public void setPosition(int x, int y){
        this.x = x;
        this.y = y;
    }
    
    public int getX(){
        return x;
    }
    
    public int getY(){
        return y;
    }
    
    public int getWidth(){
        return width;
    }
    
    public int getHeight(){
        return height;
    }

    /**
     * {@inheritDoc}
     */
    public void draw(Graphics g, int xOffset, int yOffset){
        g.setColor(color);
        g.fillRect(x-xOffset, y-yOffset, width, height);
    }
    
}
