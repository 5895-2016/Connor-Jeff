/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view.animation;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import server.model.entity.Entity;
import server.model.entity.EntityFactory;

@SuppressWarnings("serial")
public class AnimationPanel extends JPanel {

    private int xOffset;
    private int yOffset;
    private Drawable[] players;
    private Drawable[] objects;
    private Drawable[] statics;
    private BufferedImage background;
    
    /**
     * @param filename The filename for the background image.
     */
    public AnimationPanel(String filename){
        xOffset = 0;
        yOffset = 0;
        players = new Drawable[]{};
        objects = new Drawable[]{};
        statics = new Drawable[]{};
        
        try{
        File f = new File(getClass().getResource("/client/view/images/" + filename).toURI());
            background = ImageIO.read(f);
        } catch (URISyntaxException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
    
    @Override
    public void paintComponent(Graphics g){
        // Fill background
        Graphics g2 = g.create();
        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, getWidth(), getHeight());
        g2.dispose();
        
        Graphics2D g2d = (Graphics2D) g;
        AffineTransform trans = new AffineTransform(g2d.getTransform());
        trans.scale( (double) getWidth()/background.getWidth(), 
                     (double) getHeight()/background.getHeight());
        g2d.drawImage(background,
                new AffineTransformOp(trans, AffineTransformOp.TYPE_NEAREST_NEIGHBOR),
                0, 0);
        
        // draw objects
        for (Drawable d : objects){
            Graphics g4 = g.create();
            d.draw(g4, xOffset, yOffset);
            g4.dispose();
        }
        
        // draw players
        for (int i = 0; i < players.length; i++){
            Graphics g3 = g.create();
            players[i].draw(g3, xOffset, yOffset);
            g3.dispose();
        }
        
        // draw static objects
        for (Drawable d : statics){
            Graphics g5 = g.create();
            d.draw(g5, 0, 0);
            g5.dispose();
        }
    }
    
    public void setXOffset(int x){
        xOffset = x;
    }
    
    public void setYOffset(int y){
        yOffset = y;
    }
    
    /**
     * @param items The drawables representing each player in order.
     */
    public void setPlayers(Drawable[] items){
        this.players = items;
    }
    
    /**
     * @param items The drawables for all inanimate (non-player) objects.
     */
    public void setObjects(Drawable[] items){
        this.objects = items;
    }

    /**
     * @param items The drawables for all static objects. Static objects are not offset when they are drawn.
     */
    public void setStaticObjects(Drawable[] items){
        this.statics = items;
    }
    
    /**
     * Creates a {@code Drawable} from an {@code Entity}.
     * performs the conversion from game space to screen space.
     * @param e an entity.
     * @param c the drawable's colour.
     * @return a drawable representing the entity.
     */
    public Drawable createDrawable(Entity e, Color c){
        int x = transformXCoordinate(e.getX());
        int y = transformYCoordinate(e.getY());
        int width = transformXCoordinate(e.getWidth());
        int height = transformYCoordinate(e.getHeight());
        Drawable d = e.createDrawable(width, height);
        d.setPosition(x, y);
        return d;
    }
    
    /**
     * Transforms an x coordinate in model coordinates to an x in pixels (view coordinates).
     * @param modelPoint an x value in game space.
     * @return an x value in view space.
     */
    public int transformXCoordinate(double modelPoint){
        double xConversionFactor = (double) getWidth()/EntityFactory.WorldWidth;
        return (int) (modelPoint * xConversionFactor);
    }

    /**
     * Transforms a y coordinate in model coordinates to a y in pixels (view coordinates).
     * @param modelPoint a y value in game space.
     * @return a y value in view space.
     */
    public int transformYCoordinate(double modelPoint){
        double yConversionFactor = (double) getHeight()/EntityFactory.WorldHeight;
        return (int) (modelPoint * yConversionFactor);
    }
}
