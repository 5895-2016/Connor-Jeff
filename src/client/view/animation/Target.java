/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view.animation;

import java.awt.Color;
import java.awt.Graphics;

public class Target implements Drawable {
	
	private int x;
    private int y;
    private int width;
    private int height;
    
    public Target(int width, int height){
    	this.width = width;
    	this.height = height;
    }

	public void setPosition(int x, int y){
        this.x = x;
        this.y = y;
    }
    
    public int getX(){
        return x;
    }
    
    public int getY(){
        return y;
    }
    
    public int getWidth(){
        return width;
    }
    
    public int getHeight(){
        return height;
    }

    /**
	 * {@inheritDoc}
	 */
	@Override
	public void draw(Graphics g, int xOffset, int yOffset) {
        g.setColor(Color.RED);
        g.fillOval(x, y, width, height);
        
        g.setColor(Color.WHITE);
        g.fillOval(x+7, y+7, width-14, height-14);
        
        g.setColor(Color.RED);
        g.fillOval(x+16, y+16, width-32, height-32);

	}

}
