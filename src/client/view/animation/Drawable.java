/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view.animation;

import java.awt.Graphics;

public interface Drawable {
    
    public int getX();
    
    public int getY();
    
    public int getWidth();
    
    public int getHeight();
    
    public void setPosition(int x, int y);
    
    /**
     * Draws the {@code Drawable} to the screen.
     * @param g A copy of the current graphics context.
     * @param xOffset The x offset of the frame.
     * @param yOffset The y offset of the frame.
     */
    public void draw(Graphics g, int xOffset, int yOffset);

}
