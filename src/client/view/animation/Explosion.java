/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view.animation;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.imageio.ImageIO;

public class Explosion implements Drawable {

	private BufferedImage img;
	private static int ExplosionWidth = 50;
	private int x,y;
	
	public Explosion(int x, int y){
		this.x = x;
		this.y = y;
		String imageLocation = ("/client/view/images/Explosion.png");
		try{
	        File f = new File(getClass().getResource(imageLocation).toURI());
	            img = ImageIO.read(f);
	        } catch (URISyntaxException e){
	            e.printStackTrace();
	        } catch (IOException e){
	            e.printStackTrace();
	        }
	}
	
	@Override
	public int getX() {
		return x;
	}

	@Override
	public int getY() {
		return y;
	}

	@Override
	public int getWidth() {
		return ExplosionWidth;
	}

	@Override
	public int getHeight() {
		return ExplosionWidth;
	}

	@Override
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void draw(Graphics g, int xOffset, int yOffset) {
		Graphics2D g2d = (Graphics2D) g;
		AffineTransform trans = new AffineTransform(g2d.getTransform());
		trans.scale( (double) getWidth()/img.getWidth(), (double) getHeight()/img.getHeight());
		g2d.drawImage(img,
                new AffineTransformOp(trans, AffineTransformOp.TYPE_NEAREST_NEIGHBOR),
                x, y);

	}

}
