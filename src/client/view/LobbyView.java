/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;

import client.Client;
import client.controller.LobbyController;
import io.viewInput.ChatOutput;
import io.viewInput.LobbyOutput;
import io.viewInput.PlayerAddition;
import io.viewInput.ReadyOutput;
import server.model.PlayerModel;

@SuppressWarnings("serial")
public class LobbyView extends View {
	
	
	private static int chatHeight = 150, chatWidth = 250;
	private JTextField textField;
	private JTextArea textArea;
	private LobbyScoreboard scoreBoard;
	private JButton readyButton;
	private Client client;
	private JLabel[] readyLabels = new JLabel[4];
	
	private LobbyController lobbyController;
	
	public LobbyView(Client client){
	    super(Client.viewWidth, Client.viewHeight);
	    this.client = client;
		setLocation(((client.getPlayerNumber()-1) % 2) * Client.viewWidth, ((client.getPlayerNumber()-1) / 2) * Client.viewHeight);
		setTitle(client.getPlayerName());
        
		lobbyController = new LobbyController(client, this);
		
		//Ready Labels
		for (int i = 0; i < 4; i++){
			readyLabels[i] = new JLabel("READY");
			readyLabels[i].setBounds(225, 107 + (i*50),readyLabels[i].getPreferredSize().width,readyLabels[i].getPreferredSize().height);
			readyLabels[i].setVisible(false);
			add(readyLabels[i]);
		}
		
        //Text Field
        textField = new JTextField();
        textField.setBounds(0, 451, chatWidth, 25);
        Border border = BorderFactory.createLineBorder(Color.BLUE, 1);
        textField.setBorder(border);
        textField.addActionListener(lobbyController);
        add(textField);
        
        //Text Area
        textArea = new JTextArea();
        textArea.setBounds(0, 327, chatWidth, chatHeight-25);
        textArea.setBorder(border);
        textArea.setEditable(false);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        add(textArea);
        
        //ScoreBoard Positions
        scoreBoard = new LobbyScoreboard();
        scoreBoard.setLocation(20, 5);
        add(scoreBoard);
        
        //readyButton
        readyButton = new JButton("READY");
        readyButton.setBounds(315, 350, 125, 100);
        readyButton.addActionListener(lobbyController);
        readyButton.setVisible(false);
        add(readyButton);
        
        activate();
	}
    
    public void activate(){
        setVisible(true);
        lobbyController.requestScore();
    }
    
    public void deactivate(){
        setVisible(false);
        resetReady();
    }

	/**
	 * Updates the chat window with new messages.
	 * @param chatOutput - Contains the new string message.
	 */
	public void chatUpdate(ChatOutput chatOutput){
        String newMessage = chatOutput.getChatMessage();
        textArea.setText( newMessage + "\n" + textArea.getText() );
        textField.setText("");
	}
	
	/**
	 * Tells the scoreboard to update with current players.
	 * @param lobbyOutput - Contains the current player models with updated game information.
	 */
	public void lobbyUpdate(LobbyOutput lobbyOutput){
		client.updatePlayers(lobbyOutput.getPlayers());
		scoreBoard.updateScore(lobbyOutput.getPlayers());
		int i = 0;
		for (PlayerModel p : lobbyOutput.getPlayers()){
		    readyLabels[p.getPlayerNumber()-1].setBounds(225, 107 + (i*50),readyLabels[i].getPreferredSize().width,readyLabels[i].getPreferredSize().height);
		    i++;
		}
	}
	
	/**
	 * Updates the view to show what player is ready.
	 * @param input - Contains the player who is ready.
	 */
	public void updateReadyCheck(ReadyOutput input){
		int playerNumber = input.getPlayerNumber();
		readyLabels[playerNumber-1].setVisible(true);
	}
    
	/**
	 * Sets the ready labels to not visible.
	 */
    public void resetReady(){
        for (int i = 0; i < 4; i++){
            readyLabels[i].setVisible(false);
        }
    }
	
    /**
     * Tells the {@code LobbyScoreboard} to update that a player has been added
     * @param addition New player that has been added
     */
	public void showPlayer(PlayerAddition addition){
	    scoreBoard.showPanels(addition.getVisiblePlayerCount());
        if (addition.getVisiblePlayerCount() == 4){
            readyButton.setVisible(true);
        }
	}
	
	public String getChatText(){ return textField.getText(); }
	
	public JButton getReadyButton(){ return readyButton; }
	public JTextField getTextField(){ return textField; }
}
