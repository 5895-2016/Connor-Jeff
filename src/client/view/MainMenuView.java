/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view;

import javax.swing.JButton;

import client.Client;
import client.controller.MainMenuButtonListener;

@SuppressWarnings("serial")
public class MainMenuView extends View {

    static private int buttonHeight = 50, buttonWidth = 100;
	
	public MainMenuView(Client client){
	    super(Client.viewWidth, Client.viewHeight);
        setLocationRelativeTo(null);
        
        // Host button
        JButton hostButton = new JButton("HOST");
        hostButton.setBounds(((getWidth()*3)/4 - buttonWidth/2),
                             ((getHeight())/2 - buttonHeight/2),
                              buttonWidth, buttonHeight);
        hostButton.addActionListener(new MainMenuButtonListener(client, "HOST"));
        add(hostButton);
        
        // Join button
        JButton joinButton = new JButton("JOIN");
        joinButton.setBounds(((getWidth()*3)/4 - buttonWidth/2),
                             ((getHeight()*19)/32 - buttonHeight/2),
                              buttonWidth, buttonHeight);
        joinButton.addActionListener(new MainMenuButtonListener(client, "JOIN"));
        add(joinButton);
        
        // Test button
        JButton testButton = new JButton("TEST");
        testButton.setBounds(((getWidth()*3)/4 - buttonWidth/2),
                             ((getHeight()*11)/16 - buttonHeight/2),
                              buttonWidth, buttonHeight);
        testButton.addActionListener(new MainMenuButtonListener(client, "TEST"));
        add(testButton);
        
        // Panel for background
        MainMenuPanel panel = new MainMenuPanel(client);
        panel.setBounds(0, 0, getWidth(), getHeight());
        add(panel);
        
        setVisible(true);
	}
	
}