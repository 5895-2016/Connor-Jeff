/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;

import client.Client;
import client.controller.WinscreenController;
import io.viewInput.WinscreenOutput;

@SuppressWarnings("serial")
public class WinscreenView extends View {

    private JLabel[] playerNames = new JLabel[4];
    private JLabel[] scores = new JLabel[4];
    private JLabel[] places = new JLabel[4];
    private JPanel[] namePanels = new JPanel[4];
    private JPanel[] scorePanels = new JPanel[4];
    private JPanel[] placePanels = new JPanel[4];
    private JPanel[] titlePanels = new JPanel[3];
    
    public WinscreenView(Client client){
        super(Client.viewWidth, Client.viewHeight);
        setLocation(((client.getPlayerNumber()-1) % 2) * Client.viewWidth,
                ((client.getPlayerNumber()-1) / 2) * Client.viewHeight);
        setTitle("Final Results");
        
        // titles for each table column.
        JLabel nameTitle = new JLabel("PLAYER");
        JLabel rankTitle = new JLabel("RANK");
        JLabel scoreTitle = new JLabel("FINAL SCORE");
        JLabel[] titles = {nameTitle, scoreTitle, rankTitle};
        for(int i = 0; i < 3; i++){
            titlePanels[i] = new JPanel();
            titlePanels[i].setBounds(25 + 150*i, 50, 150, 50);
            titlePanels[i].add(titles[i]);
            titlePanels[i].setBackground(new Color(236, 100, 75));
            this.add(titlePanels[i]);
        }
        
        // panels representing each cell in the table
        for(int i = 0; i < 4; i++){
            playerNames[i] = new JLabel();
            scores[i] = new JLabel();
            places[i] = new JLabel();
            namePanels[i] = new JPanel();
            scorePanels[i] = new JPanel();
            placePanels[i] = new JPanel();
            
            playerNames[i].setText("Player" + (i+1));
            scores[i].setText("0");
            places[i].setText("1");
            
            namePanels[i].setBounds(25, 110 + (50*i), 150, 50);
            namePanels[i].add(playerNames[i]);
            namePanels[i].setBackground(playerColors[i]);
            scorePanels[i].setBounds(175, 110 + (50*i), 150, 50);
            scorePanels[i].add(scores[i]);
            scorePanels[i].setBackground(playerColors[i]);
            placePanels[i].setBounds(325, 110 + (50*i), 150, 50);
            placePanels[i].add(places[i]);
            placePanels[i].setBackground(playerColors[i]);
            
            this.add(namePanels[i]);
            this.add(scorePanels[i]);
            this.add(placePanels[i]);
        }

        // controller
        WinscreenController controller = new WinscreenController(client);
        
        setVisible(true);
        controller.requestScore();
    }

    /**
     * Updates the win screen components with the most recent data from the model.
     * @param winscreenOutput Data output from the model.
     */
    public void winscreenUpdate(WinscreenOutput winscreenOutput){
        for (int i = 0; i < winscreenOutput.getPlayers().length; i++){
            playerNames[i].setText(winscreenOutput.getPlayers()[i].getName());
            scores[i].setText(Integer.toString(winscreenOutput.getPlayers()[i].getScore()));
            places[i].setText(Integer.toString(winscreenOutput.getPlayers()[i].getPosition()));
            namePanels[i].setBackground(playerColors[winscreenOutput.getPlayers()[i].getPlayerNumber()-1]);
            scorePanels[i].setBackground(playerColors[winscreenOutput.getPlayers()[i].getPlayerNumber()-1]);
            placePanels[i].setBackground(playerColors[winscreenOutput.getPlayers()[i].getPlayerNumber()-1]);
        }
    }

}
