/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;

import server.model.PlayerModel;

@SuppressWarnings("serial")
public class LobbyScoreboard extends JPanel{

	private JLabel[] places = new JLabel[4];
	private JPanel[] placePanels = new JPanel[4];	

	private JLabel[] playerNames = new JLabel[4];
	private JPanel[] namePanels = new JPanel[4];	

	private JLabel[] scores = new JLabel[4];
	private JPanel[] scorePanels = new JPanel[4];
	
	private JPanel[] titlePanels = new JPanel[4];
	
	private int[] playerNumbers = {1,2,3,4};
	private PlayerModel players[];
	
	public LobbyScoreboard(){
		
		setSize(450,320);
		setLayout(null);
		
		// titles for each table column.
		JLabel nameTitle = new JLabel("POSITION");
		JLabel rankTitle = new JLabel("SCORE");
		JLabel scoreTitle = new JLabel("PLAYER");
		JLabel[] titles = {nameTitle, scoreTitle, rankTitle};
		for(int i = 0; i < 3; i++){
			titlePanels[i] = new JPanel();
			titlePanels[i].setBounds(150*i, 0, 150, 50);
			titlePanels[i].add(titles[i]);
			titlePanels[i].setBackground(new Color(236, 100, 75));
			this.add(titlePanels[i]);
		}
		
		for(int i = 0; i < 4; i++){
			playerNames[i] = new JLabel();
			scores[i] = new JLabel();
			places[i] = new JLabel();
			
			namePanels[i] = new JPanel();
			scorePanels[i] = new JPanel();
			placePanels[i] = new JPanel();
			
			
			playerNames[i].setText("Player" + (i+1));
			scores[i].setText("0");
			places[i].setText("1");
			
			placePanels[i].setBounds(0, 75 + (50*i), 150, 50);
			placePanels[i].add(places[i]);
			placePanels[i].setBackground(View.playerColors[i]);
			namePanels[i].setBounds(150, 75 + (50*i), 150, 50);
			namePanels[i].add(playerNames[i]);
			namePanels[i].setBackground(View.playerColors[i]);
			scorePanels[i].setBounds(300, 75 + (50*i), 150, 50);
			scorePanels[i].add(scores[i]);
			scorePanels[i].setBackground(View.playerColors[i]);
			
			this.add(namePanels[i]);
			this.add(scorePanels[i]);
			this.add(placePanels[i]);
			

			namePanels[i].setVisible(false);
			scorePanels[i].setVisible(false);
			placePanels[i].setVisible(false);
		}

		namePanels[0].setVisible(true);
		scorePanels[0].setVisible(true);
		placePanels[0].setVisible(true);
		setVisible(true);
	}
	

	/**
	 * Updates the player models and then refreshes the scoreboard.
	 * @param players
	 */
	public void updateScore(PlayerModel players[]){
		this.players = players;
		refreshScoreboard();
	}
	
	/**
	 * Updates the labels in the scoreboard with the current players information.
	 */
	private void refreshScoreboard(){
		int j = 0;
		for (PlayerModel player : players){
			scores[j].setText(Integer.toString(player.getScore()));
			playerNames[j].setText(player.getName());
			places[j].setText(Integer.toString(player.getPosition()));
			playerNumbers[j] = player.getPlayerNumber();
            namePanels[j].setBackground(View.playerColors[player.getPlayerNumber()-1]);
            scorePanels[j].setBackground(View.playerColors[player.getPlayerNumber()-1]);
            placePanels[j].setBackground(View.playerColors[player.getPlayerNumber()-1]);
			j++;
		}
	}
	
	/**
	 * Sets the scoreboard panels to visible depending on the amount of players given.
	 * @param visiblePlayerCount Number of players to show.
	 */
	public void showPanels(int visiblePlayerCount){
	    for (int i = 0; i < visiblePlayerCount; i++){
	        namePanels[i].setVisible(true);
	        scorePanels[i].setVisible(true);
	        placePanels[i].setVisible(true);
	    }
	}
}
