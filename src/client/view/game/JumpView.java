/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view.game;

import client.Client;
import client.view.animation.AnimationPanel;
import io.viewInput.GameSnapshot;

@SuppressWarnings("serial")
public class JumpView extends GameView {
    
    private Client client;
    private int yOffset;
    
    public JumpView(Client client){
        super(client);
        this.client = client;
        yOffset = 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AnimationPanel createAnimationPanel() {
        return new AnimationPanel("jumpbg.png");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void applyOffset(GameSnapshot snapshot) {
        double playerYPosition = snapshot.getPlayerEntities()[client.getPlayerNumber()-1].getY();
        if (panel.transformYCoordinate(playerYPosition) < yOffset + panel.getHeight()/2){
            yOffset = panel.transformYCoordinate(playerYPosition) - panel.getHeight()/2;
            panel.setYOffset(yOffset);
        } else if (panel.transformYCoordinate(playerYPosition) > yOffset + 17*panel.getHeight()/20){
            yOffset = panel.transformYCoordinate(playerYPosition) - 17*panel.getHeight()/20;
            panel.setYOffset(yOffset);
        }
    }

}
