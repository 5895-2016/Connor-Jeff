/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view.game;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import client.Client;
import client.view.animation.AnimationPanel;
import io.viewInput.GameSnapshot;

@SuppressWarnings("serial")
public class TargetShooterView extends GameView {
	
	public TargetShooterView(Client client) {
		super(client);
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Image image = toolkit.getImage("/client/view/images/Crosshair" + client.getPlayerNumber() + ".png");
		Cursor c = toolkit.createCustomCursor(image , new Point(0, 
		           0), "img");
		super.setCursor (c);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AnimationPanel createAnimationPanel() {
		return new AnimationPanel("TargetShooterBackground.png");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void applyOffset(GameSnapshot snapshot) {
		
	}

}
