/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view.game;

import client.Client;
import client.view.animation.AnimationPanel;
import io.viewInput.GameSnapshot;

@SuppressWarnings("serial")
public class PongView extends GameView {
    
    public PongView(Client client){
        super(client);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AnimationPanel createAnimationPanel(){
        return new AnimationPanel("pongNet.png");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void applyOffset(GameSnapshot snapshot){
        // pass
    }

}
