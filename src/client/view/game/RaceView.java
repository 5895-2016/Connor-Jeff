/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.view.game;

import client.Client;
import client.view.animation.AnimationPanel;
import io.viewInput.GameSnapshot;

@SuppressWarnings("serial")
public class RaceView extends GameView {
    
    private Client client;
    
    public RaceView(Client client){
        super(client);
        this.client = client;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AnimationPanel createAnimationPanel(){
        return new AnimationPanel("sanicbg.png");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void applyOffset(GameSnapshot snapshot){
        double playerXPosition = snapshot.getPlayerEntities()[client.getPlayerNumber()-1].getX();
        panel.setXOffset(panel.transformXCoordinate(playerXPosition) - (panel.getWidth()/2));
    }

}
