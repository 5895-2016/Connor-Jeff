/****************************************************
 * @author Jeff Morris - 201125150 (jtm437@mun.ca)
 * 		   Connor Whalen - 201335544 (caw742@mun.ca) 
 ****************************************************/

package client.view.game;

import java.awt.BorderLayout;
import java.awt.Color;

import client.Client;
import client.controller.GameController;
import client.view.View;
import client.view.animation.AnimationPanel;
import client.view.animation.Drawable;
import io.viewInput.GameSnapshot;

@SuppressWarnings("serial")
public abstract class GameView extends View {

    protected AnimationPanel panel;
    
    public GameView(Client client){
        super(Client.viewWidth, Client.viewHeight);
        setLocation(((client.getPlayerNumber()-1) % 2) * Client.viewWidth, ((client.getPlayerNumber()-1) / 2) * Client.viewHeight);
        setLayout(new BorderLayout());
        
        panel = createAnimationPanel();
        GameController controller = new GameController(client);
        this.addKeyListener(controller);
        panel.addMouseListener(controller);
        panel.addMouseMotionListener(controller);
        add(panel, BorderLayout.CENTER);
        panel.repaint();
        
        setVisible(true);
    }
    
    /**
     * Takes each entity in the {@code GameSnapshot}, creates their equivalent drawables, 
     * sends them to the panel, then tells the panel to repaint itself.
     * @param snapshot A snapshot containing each entity to be drawn.
     */
    // TODO: Combine the game and preview animation methods and their respective snapshot classes.
    public void gameUpdate(GameSnapshot snapshot){
        Drawable[] players = new Drawable[snapshot.getPlayerEntities().length];
        Drawable[] objects = new Drawable[snapshot.getInanimateEntities().length];
        Drawable[] statics = new Drawable[snapshot.getStaticEntities().length];
        for (int i = 0; i < players.length; i++) {
            players[i] = panel.createDrawable(snapshot.getPlayerEntities()[i], View.playerColors[i]);
        }
        for (int i = 0; i < objects.length; i++) {
            objects[i] = panel.createDrawable(snapshot.getInanimateEntities()[i], Color.BLACK);
        }
        for (int i = 0; i < statics.length; i++) {
            statics[i] = panel.createDrawable(snapshot.getStaticEntities()[i], Color.BLACK);
        }
        applyOffset(snapshot);
        panel.setPlayers(players);
        panel.setObjects(objects);
        panel.setStaticObjects(statics);
        panel.repaint();
    }

    /**
     * Creates the animation panel with the correct background image.
     * @return An AnimationPanel with the correct background image.
     */
    public abstract AnimationPanel createAnimationPanel();

    /**
     * Applies an offset to the AnimationPanel to follow entities that move outside the initial frame.
     * @param snapshot a snapshot of the current entity positions.
     */
    public abstract void applyOffset(GameSnapshot snapshot);

}
