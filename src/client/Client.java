/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javax.swing.SwingUtilities;

import client.view.LobbyView;
import client.view.View;
import client.view.ViewFactory;
import game.GameState;
import game.MiniGame;
import io.modelInput.ModelInput;
import io.viewInput.ViewInput;
import server.Server;
import server.ServerInterface;
import server.model.PlayerModel;

public class Client extends UnicastRemoteObject implements ClientInterface {
    
    private static final long serialVersionUID = 100L;
	
	private GameState currentState;
	private View currentView;
    private LobbyView lobbyView;
	private ViewFactory viewFactory;
	public static int viewHeight = 500, viewWidth = 500;
	private int playerNumber;
	private String playerName = "";
	private ServerInterface server;
	private PlayerModel[] players;
	
	/**
	 * Constructor for Client
	 * @param playerNumber - The player number associated with this client.
	 * @param server - The server that creates this Client.
	 */
	public Client() throws RemoteException{
	    viewFactory = new ViewFactory(this);
		changeState(GameState.MAINMENU);
	}

	// For test purposes only
    public Client(Server s, int num) throws RemoteException{
        server = s;
        setPlayerNumber(num);
        viewFactory = new ViewFactory(this);
        changeState(GameState.MAINMENU);
    }
	
	/**
	 * Used by the host client to create the server.
	 */
	synchronized public void createServer(){
		try {
            server = new Server(this, this.getPlayerName());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        notifyAll();
	}
	
	// For test purposes only
	synchronized public void createTestServer(){
	    try {
            server = new Server(this, 1);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
	    notifyAll();
	}
	
	// For test purposes only
	synchronized public void createTestClients(){
        try {
            Client player2 = new Client((Server) server, 2);
            Client player3 = new Client((Server) server, 3);
            Client player4 = new Client((Server) server, 4);
            for (Client c : new Client[]{player2, player3, player4}){
                Thread addClientThread = new Thread(new Runnable(){
                    public void run(){
                        try {
                         server.addClient(c, "Player4");
                     } catch (RemoteException e) {
                         e.printStackTrace();
                     }
                    }
                 });
                 addClientThread.start();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
	}
	
	synchronized public void findServer(String host){
	    String name = "rmi://" + host + ":" + Server.port + "/" + Server.networkName;
	    try{
	        server = (ServerInterface) Naming.lookup(name);
	        Client c = this;
	        Thread addClientThread = new Thread(new Runnable(){
	           public void run(){
	               try {
                    server.addClient(c, c.getPlayerName());
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
	           }
	        });
	        addClientThread.start();
	    } catch (RemoteException | MalformedURLException | NotBoundException e){
	        e.printStackTrace();
	    } catch (ArrayIndexOutOfBoundsException e){
	        // TODO: do something reasonable when the lobby is already full
	        e.printStackTrace();
	    }
        notifyAll();
	}
	
	/**
	 * Changes the current state and current view.
	 * @param newState
	 */
	public void changeState(GameState newState){
        if (currentState == null || newState != currentState){
	        currentState = newState;
    	    if (currentView != null){
    	        if (lobbyView != null && currentView == lobbyView){
    	            lobbyView.deactivate();
    	        } else{
    	            currentView.dispose();
    	        }
    	    }
            if (newState == GameState.GAMELOBBY){
                if (lobbyView != null){
                    currentView = lobbyView;
                    lobbyView.activate();
                } else{
                    currentView = viewFactory.create(currentState);
                    lobbyView = (LobbyView) currentView;
                }
            }
            else{
    	        currentView = viewFactory.create(currentState);
            }
            
	        if (server != null){
	            try {
                    server.clientReady(getPlayerNumber());
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
	        }
        }
	}
	
	public void setPlayerNumber(int number){ playerNumber = number; }
	
	public int getPlayerNumber(){ return playerNumber; }
	
	/**
	 * Sends input to the server to update the model.
	 * @param modelInput
	 */
	public void sendModelInput(ModelInput modelInput){
	    Thread serverUpdateThread = new Thread(new Runnable(){
	       public void run(){
	           waitForServerAndUpdate(modelInput);
	       }
	    });
	    serverUpdateThread.start();
	}
	
	/**
	 * Waits for the server to available then sends update.
	 * @param modelInput
	 */
	synchronized private void waitForServerAndUpdate(ModelInput modelInput){
	    while (server == null){
	        try{
	            wait();
	        } catch (InterruptedException e){
	            e.printStackTrace();
	        }
	    }
        try {
            server.updateModel(modelInput);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
	}
	
	synchronized public void setCurrentMinigame(MiniGame game){
	    viewFactory.setCurrentGame(game);
	}

	public void setPlayerName(String name){ this.playerName = name; }
	
	public String getPlayerName(){ return this.playerName; }
	
	/**
	 * Updates the current view with output from the server.
	 * @param viewInput
	 */
	public void updateView(ViewInput viewInput){
		currentView.updateView(viewInput);
	}
	
	public static void main(String args[]){
		SwingUtilities.invokeLater(new Runnable(){
            public void run(){
                try{
                    new Client();
                } catch (RemoteException e){
                    e.printStackTrace();
                }
            }
        });
	}
	
	public void updatePlayers(PlayerModel[] players){ this.players = players; }
	
	public PlayerModel[] getPlayers(){ return players; }
	
}
