/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.controller;

import java.awt.event.ActionEvent;

import client.Client;
import client.view.LobbyView;
import io.modelInput.ChatInput;
import io.modelInput.ReadyInput;
import io.modelInput.ScoreRequest;

public class LobbyController implements java.awt.event.ActionListener {

	Client client;
	LobbyView lobbyView;
	
	public LobbyController(Client client, LobbyView lobbyView){
		this.client = client;
		this.lobbyView = lobbyView;
	}
	
	@Override
	public void actionPerformed(ActionEvent e){
	    // If a chat message was typed in
		if(e.getSource() == lobbyView.getTextField()){
			this.sendMessage(client.getPlayerName() +": " + lobbyView.getChatText());
		}
		// If the ready button was clicked
		if(e.getSource() == lobbyView.getReadyButton()){
			this.readyUp();
		}
	}
	
	/**
	 * Sends a {@code ChatInput} to the model.
	 * @param message The chat message being sent.
	 */
	public void sendMessage(String message){
		ChatInput input = new ChatInput(message);
		client.sendModelInput(input);
	}
	
	/**
	 * Sends a score request to the model.
	 */
	public void requestScore(){
        client.sendModelInput(new ScoreRequest());
	}
	
	/**
	 * Sends a {@code ReadyInput} to the model.
	 */
	public void readyUp(){ 
		ReadyInput input = new ReadyInput(client.getPlayerNumber());
		client.sendModelInput(input);
	}
	
}
