/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.controller;

import client.Client;
import io.modelInput.ScoreRequest;

public class WinscreenController {
    
    private Client client;
    
    public WinscreenController(Client client){
        this.client = client;
    }

    /**
     * Sends a score request to the model.
     */
    public void requestScore(){
        client.sendModelInput(new ScoreRequest());
    }
}
