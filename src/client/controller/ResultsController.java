/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import client.Client;
import io.modelInput.ReadyInput;
import io.modelInput.ScoreRequest;

public class ResultsController implements ActionListener {
    
    private Client client;
    
    public ResultsController(Client client){
        this.client = client;
    }
    
    /**
     * Sends a score request to the model.
     */
    public void requestScore(){
        client.sendModelInput(new ScoreRequest());
    }
    
    /**
     * Sends a {@code ReadyInput} to the model if the "ready" button is clicked.
     */
    public void actionPerformed(ActionEvent e){
        client.sendModelInput(new ReadyInput(client.getPlayerNumber()));
    }

}
