/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.controller;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import client.Client;

public class MainMenuButtonListener implements ActionListener{
	
	Client client;
	String button;
	String option;
	JFrame nameTextFrame;
    JTextField nameField;
    JTextField ipField;
	
	public MainMenuButtonListener(Client client, String button){
		this.client = client;
		this.button = button;
		this.option = button;
	}
	
	/**
	 * Creates a server if the "host" button is clicked.
	 * Locates a server if the "join" button is clicked.
	 * Creates a non-remote server if the "test" button is clicked.
	 */
	@Override
	public void actionPerformed(ActionEvent e){
        if (button.matches("HOST")){
            namePrompt();
            button = "NAME";
        } else if (button.matches("JOIN")){
            nameAndIpPrompt();
            button = "NAME";
        } else if (button.matches("TEST")){
            client.createTestServer();
            client.createTestClients();
        } else if (button.matches("NAME")){
        	client.setPlayerName(nameField.getText());
        	if (option.matches("HOST")) {
                nameTextFrame.dispose();
        	    ipDisplay();
        	    client.createServer();
        	}
        	else if (option.matches("JOIN")) {
                nameTextFrame.dispose();
                client.findServer(ipField.getText());
        	}
        }
	}
	
	/**
	 * Creates a {@code JFrame} with a {@code JTextField} for a user to input a user name.
	 */
	private void namePrompt(){

        nameTextFrame = new JFrame("Please enter a name");
        nameTextFrame.setBounds(400, 300, 300, 60);
        nameTextFrame.setLayout(new GridLayout(1, 1));
        nameField = new JTextField();
        nameField.addActionListener(this);
        nameTextFrame.add(nameField);
        nameTextFrame.setVisible(true);
	}
	
	/**
	 * Creates a {@code JFrame} with two {@code JTextField}'s for a user to input a user name and IP to connect
	 * to the host.
	 */
	private void nameAndIpPrompt(){
        nameTextFrame = new JFrame("Please enter your name and the host's ip address");
        nameTextFrame.setBounds(400, 300, 300, 100);
        nameTextFrame.setLayout(new GridLayout(2, 1));
        nameField = new JTextField("name");
        nameField.addActionListener(this);
        ipField = new JTextField("ip");
        ipField.addActionListener(this);
        nameTextFrame.add(nameField);
        nameTextFrame.add(ipField);
        nameTextFrame.setVisible(true);
	}
	
	/**
	 * Displays a {@code JFrame} with a {@code JLabel} that contains the users IP Address.
	 */
	private void ipDisplay(){
	    nameTextFrame = new JFrame("This is your ip address");
	    nameTextFrame.setBounds(400, 300, 300, 60);
	    try{
	        nameTextFrame.add(new JLabel(InetAddress.getLocalHost().getHostAddress()));
	    } catch (UnknownHostException e){
	        e.printStackTrace();
	    }
	    nameTextFrame.setVisible(true);
	}
	
}