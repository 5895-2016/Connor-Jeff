/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import client.Client;
import io.modelInput.ModelInput;
import io.modelInput.ReadyInput;


public class PreviewButtonListener implements ActionListener{
    
    Client client;
    String button;
    
    public PreviewButtonListener(Client client, String button){
        this.client = client;
        this.button = button;
    }
    
    /**
     * Sends a {@code ReadyInput} if the "ready" button is clicked.
     */
    @Override
    public void actionPerformed(ActionEvent e){
        if (button.matches("READY")){
            ModelInput input = new ReadyInput(client.getPlayerNumber());
            client.sendModelInput(input);
        }
    }
    
}
