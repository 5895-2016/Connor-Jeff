/**
 * <h4>author</h4> Jeff Morris - 201125150 (jtm437@mun.ca)
 * <h4>author</h4> Connor Whalen - 201335544 (caw742@mun.ca) 
 * <h4>date</h4> Mar 11, 2016
 */

package client.controller;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import client.Client;
import io.modelInput.GameInput;

public class GameController implements KeyListener, MouseListener, MouseMotionListener{
    
    Client client;

    boolean up;
    boolean down;
    boolean left;
    boolean right;
    boolean space;
    
    double count;
    Point mouseMovePosition;
    Point mouseClickPosition;
    
    public GameController(Client client){
        this.client = client;
        count = 0;
    }
    
    /**
     * Sets the direction booleans to true based on which key was pressed.
     */
    @Override
    public void keyPressed(KeyEvent e){
        if (e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode() == KeyEvent.VK_W){
            up = true;
            sendUpdate();
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_S){
            down = true;
            sendUpdate();
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_A){
            left = true;
            sendUpdate();
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT || e.getKeyCode() == KeyEvent.VK_D){
            right = true;
            sendUpdate();
        } else if (e.getKeyCode() == KeyEvent.VK_SPACE){
            space = true;
            sendUpdate();
        }
    }

    @Override
    public void keyTyped(KeyEvent e){
        
    }

    /**
     * Sets the direction booleans to false based on which key was released.
     */
    @Override
    public void keyReleased(KeyEvent e){
        if (e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode() == KeyEvent.VK_W){
            up = false;
            sendUpdate();
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_S){
            down = false;
            sendUpdate();
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_A){
            left = false;
            sendUpdate();
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT || e.getKeyCode() == KeyEvent.VK_D){
            right = false;
            sendUpdate();
        } else if (e.getKeyCode() == KeyEvent.VK_SPACE){
            space = false;
            sendUpdate();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e){

    }

    @Override
    public void mouseEntered(MouseEvent e){
        
    }

    @Override
    public void mouseExited(MouseEvent e){
        
    }

    @Override
    public void mouseReleased(MouseEvent e){
        
    }

    /**
     * Sets the coordinates of where the mouse was clicked and sends update to model.
     */
    @Override
    public void mousePressed(MouseEvent e){
    	mouseClickPosition = e.getPoint();
        sendUpdate(true);
    }

    /**
     * Sets the coordinates for where the mouse was moved to and sends update to model.
     */
    @Override
    public void mouseMoved(MouseEvent e){
        mouseMovePosition = e.getPoint();
        sendUpdate();
    }

    @Override
    public void mouseDragged(MouseEvent e){

    }
    
    /**
     * sends a {@code GameInput} containing the current state of 
     * {@code up}, {@code down}, {@code left}, and {@code right}
     */
    private void sendUpdate(){
        client.sendModelInput(new GameInput(up, down, left, right, space, mouseMovePosition, client.getPlayerNumber()));
    }
    
    private void sendUpdate(boolean clicked){
    	client.sendModelInput(new GameInput(up, down, left, right, space, mouseMovePosition, mouseClickPosition, client.getPlayerNumber(), clicked));

    }

}
